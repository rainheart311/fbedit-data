#include Once "windows.bi"
#include Once "crt/stdio.bi"
#include Once "crt/string.bi"
#include Once "asm51.bi"

ReDim Shared Labels(100) As LabelType
Dim Shared LabelCnt As Long = 0
ReDim Shared JmpLabels(100) As JmpLabelType
Dim Shared JmpLabelCnt As Long = 0
ReDim Shared AsmData(1000) As AsmDataType
Dim Shared AsmCnt As Long = 0
ReDim Shared EquData(100) As EquDataType
Dim Shared EquCnt As Long = 0

Dim Shared nline As Long = 1
Dim Shared curAddr As Long = 0
Dim Shared IsEnd As Long = 0		'结束标志，后续的不编译
Dim Shared IsError As Long = 0		'错误标志，停止编译，并输出错误信息：行 + 错误原因

'跳过空格和TAB
Sub SkipSpace(ByRef pStr As UByte Ptr)
	If pStr = 0 Then Exit Sub
	While *pStr = Asc(" ") OrElse *pStr = VK_TAB
		pStr += 1
	Wend	
End Sub

'跳过一行
Sub SkipLine(ByRef pStr As UByte Ptr)
	If pStr = 0 Then Exit Sub
	While *pStr
		If *pStr = VK_RETURN Then Exit While
		If *pStr = 10 Then Exit While
		pStr += 1
	Wend
	pStr += 1
	If *pStr = 10 Then pStr += 1
End Sub

'查找并Include文件
Function FindInclude(ByRef asmFile As Const ZString,ByRef pStr As UByte Ptr) As String
	If pStr = 0 Then Exit Function
	
	Dim pFind As ZString Ptr = @"#INCLUDE"
	Dim IsFind As Boolean = FALSE	
'查找#include
	While *pStr
		Dim nByte As UByte = *pStr
		If nByte >= Asc("a") AndAlso nByte <= Asc("z") Then '转大写
			nByte And= &H5F
		EndIf
		If nByte <> *pFind Then Exit While '不相同则跳出
		pFind += 1
		pStr += 1
		If *pFind = 0 Then
			IsFind = TRUE
		EndIf
		If *pStr = Asc(" ") OrElse *pStr = VK_RETURN OrElse *pStr = 10 Then Exit While '
	Wend
'找到
	If IsFind Then
		Dim n As Long = 0
		SkipSpace(pStr)
		If *pStr = Asc("""") Then 
			pStr += 1
			Dim p As ZString Ptr = pStr
			While *pStr <> Asc("""") 
				n += 1
				pStr += 1
			Wend
			pStr += 1  '跳过 "
			If n Then
				Return Mid(*p,1,n)
			EndIf
		EndIf
	EndIf
	Return ""
End Function

Function GetWord(ByVal pStr As UByte Ptr) As Long
	If pStr = 0 Then Exit Function
	Dim i As Long = 0
	While *pStr
		If *pStr = Asc(" ") Then Exit While
		If *pStr = VK_TAB Then Exit While
		If *pStr = Asc(",") Then Exit While
		If *pStr = Asc(";") Then Exit While
		If *pStr = VK_RETURN Then Exit While
		If *pStr = 10 Then Exit While
		pStr += 1
		i += 1
	Wend
	Return i
End Function

Sub AddUperWord(ByRef pStr As UByte Ptr,ByVal pBuf As UByte Ptr,ByVal n As Long)
	While n
		Dim nByte As UByte = *pStr
		If nByte >= Asc("a") AndAlso *pStr <= Asc("z") Then	'转大写
			nByte And= &H5F
		EndIf
		*pBuf = nByte '存储
		pBuf += 1
		pStr += 1
		n -= 1
	Wend
	*pBuf = 0 '结束
End Sub

Sub AddLabels(ByVal szLabel As ZString Ptr,ByVal Addr As Long)
	Static defLabelSize As Long = 100
	If LabelCnt > defLabelSize Then '分配一个固定长度块，减少分配次数
		defLabelSize += 100
		ReDim Preserve Labels(defLabelSize) As LabelType
	EndIf
	
	Labels(LabelCnt).szLabel = *szLabel
	Labels(LabelCnt).Addr = Addr
	LabelCnt += 1
End Sub

Sub AddJmpLabels(ByRef szOperand As String,ByVal szLabel As ZString Ptr,ByVal Addr As Long,ByVal Idx As Long)
	Static defLabelSize As Long = 100
	If JmpLabelCnt > defLabelSize Then '分配一个固定长度块，减少分配次数
		defLabelSize += 100
		ReDim Preserve JmpLabels(defLabelSize) As JmpLabelType
	EndIf
	JmpLabels(JmpLabelCnt).szOperand = szOperand
	JmpLabels(JmpLabelCnt).szLabel = *szLabel
	JmpLabels(JmpLabelCnt).Addr = Addr
	JmpLabels(JmpLabelCnt).Idx = Idx
	JmpLabelCnt += 1
End Sub

Function AddEquDatas(ByVal szEquName As ZString Ptr,ByVal szValue As ZString Ptr,ByVal nType As EquDataEnum) As Long
	Static defEquSize As Long = 100
	If EquCnt >= defEquSize Then
		defEquSize += 100
		ReDim Preserve EquData(defEquSize) As EquDataType
	EndIf
	'查找，防止重复存储
	Dim i As Long
	If EquCnt Then
		Do
			If EquData(i).szEquName = *szEquName Then Exit Do '
			i += 1
			If i >= EquCnt Then Exit Do
		Loop
	EndIf
	'存储
	If i >= EquCnt Then
		EquData(EquCnt).szEquName = *szEquName	
		EquData(EquCnt).szData = *szValue
		EquData(EquCnt).nType = nType
		EquCnt += 1
	Else
	#ifdef _PRINT_ERRORS
		printf(!"第%d行:重复定义.\r\n",nline)  
	#endif	
		Return 0
	EndIf
	Return 1
End Function

Function AddAsmData(ByVal szAsmData As ZString Ptr,ByVal Operand As UByte,ByVal Addr As Long) As Long
	Static defAsmSize As Long = 1000
	If AsmCnt > defAsmSize Then
		defAsmSize += 1000
		ReDim AsmData(defAsmSize) As AsmDataType
	EndIf
	AsmData(AsmCnt).szAsm = *szAsmData
	AsmData(AsmCnt).Operand = Operand
	AsmData(AsmCnt).Addr = Addr
	Function = AsmCnt
    AsmCnt += 1
End Function

Function GetUperWordToBuf(ByRef pStr As UByte Ptr,szBuf() As UByte) As Long
	Dim n As Long
	SkipSpace(pStr)
	n = GetWord(pStr)
	If n Then
		ReDim szBuf(n + 1) As UByte
		AddUperWord(pStr,@szBuf(0),n)
	EndIf
	Return n
End Function

Function FindDefines(ByVal pBuf As ZString Ptr,ByRef n As Long,ByVal nType As EquDataEnum = ENUM_NULL) As Long
	If EquCnt Then
		Dim i As Long = 0,j As Long
		For i = 0 To EquCnt - 1
			If EquData(i).szEquName = *pBuf Then Exit For 
		Next
		If i < EquCnt Then
			Dim t As Long = InStr(EquData(i).szData,".")
			If t Then
				Dim s As String = Left(EquData(i).szData,t - 1)
				If Right(s,1) = "H" Then
					n = Val("&H" & s)
				ElseIf Right(s,1) = "B" Then
					n = Val("&B" & s)
				Else
					For j = 0 To Len(s)
						If isdigit(s[j]) = 0 Then Exit For 						
					Next
					If j >= Len(s) Then
						n = Val(s)
					Else
						n = 0
						Return 0
					EndIf
				EndIf
				n += Val(Mid(EquData(i).szData,t + 1))
			Else
				If Right(EquData(i).szData,1) = "H" Then
					n = Val("&H" & EquData(i).szData)
				ElseIf Right(EquData(i).szData,1) = "B" Then
					n = Val("&B" & EquData(i).szData)
				Else
					For j = 0 To Len(EquData(i).szData)
						If isdigit(EquData(i).szData[j]) = 0 Then Exit For 						
					Next
					If j >= Len(EquData(i).szData) Then
						n = Val(EquData(i).szData)
					Else
						n = 0
						Return 0
					EndIf
				EndIf
			EndIf
		Else
			Dim t As Long = InStr(*pBuf,".")	'=> P0.1
			If t = 0 Then
				t = InStr(*pBuf,"^")			'=>	P0^1
			EndIf
			If t Then
				Dim s As String = Left(*pBuf,t - 1)
				For i = 0 To EquCnt - 1
					If EquData(i).szEquName = s Then Exit For 
				Next
				If i < EquCnt Then
					If Right(EquData(i).szData,1) = "H" Then			'80H
						n = Val("&H" & EquData(i).szData)
					ElseIf Right(EquData(i).szData,1) = "B" Then		'10000000B
						n = Val("&B" & EquData(i).szData)
					Else
						For j = 0 To Len(EquData(i).szData)				'128
							If isdigit(EquData(i).szData[j]) = 0 Then Exit For 						
						Next
						If j >= Len(EquData(i).szData) Then
							n = Val(EquData(i).szData)
						Else
							n = 0
							Return 0
						EndIf
					EndIf
					n += Val(Mid(*pBuf,t + 1))							'&H80 + 1   => P0.1
				Else
					Return 0
				EndIf
			Else
				Return 0
			EndIf
		EndIf
	Else
		Return 0
	EndIf
	Return 1
End Function

Function GetAddress(ByVal pBuf As UByte Ptr,ByRef n As Long,ByVal nType As EquDataEnum = ENUM_NULL) As Long
	Dim i As Long
	If pBuf[n - 1] = Asc("H") Then										'H 结尾 十六进制
		If isdigit(pBuf[0]) Then										'且第一位必须是数字
			n = Val("&H" & Left(*Cast(ZString Ptr,pBuf),n-1))
		Else
			If FindDefines(pBuf,n) = 0 Then
				n = 0
				Return 0
			EndIf
		EndIf
	ElseIf pBuf[n - 1] = Asc("B") Then									'B 结尾 二进制
		If isdigit(pBuf[0]) Then
			n = Val("&B" & Left(*Cast(ZString Ptr,pBuf),n-1))
		Else
			If FindDefines(pBuf,n) = 0 Then
				n = 0
				Return 0
			EndIf
		EndIf
	ElseIf pBuf[0] = Asc("0") AndAlso pBuf[1] = Asc("X") Then			'0x开头 十六进制
		pBuf += 2
		n = Val("&H" & Left(*Cast(ZString Ptr,pBuf),n-2))
	Else																'十进制数或者EQU定义的值
		For i = 0 To n - 1
			If isdigit(pBuf[i]) = 0 Then Exit For 						
		Next
		If i >= n Then
			n = Val(*Cast(ZString Ptr,pBuf))
		Else
			If FindDefines(pBuf,n) = 0 Then
				n = 0
				Return 0
			EndIf			
		EndIf
	EndIf
	Return 1
End Function

Function GetValue(ByVal pBuf As UByte Ptr,ByRef n As Long) As Long
	If *pBuf = Asc("#") Then 
		pBuf += 1 
		n -= 1
	EndIf
	If pBuf[n - 1] = Asc("H") Then 									'H 结尾 十六进制 
		n = Val("&H" & Left(*Cast(ZString Ptr,pBuf),n-1))
	ElseIf pBuf[n - 1] = Asc("B") Then								'B 结尾 二进制
		n = Val("&B" & Left(*Cast(ZString Ptr,pBuf),n-1))
	ElseIf pBuf[0] = Asc("0") AndAlso pBuf[1] = Asc("X") Then			'0x开头 十六进制
		pBuf += 2
		n = Val("&H" & Left(*Cast(ZString Ptr,pBuf),n-2))
	Else 															'十进制数
		Dim i As Long
		For i = 1 To n - 1
			If isdigit(pBuf[i]) = 0 Then Exit For 
		Next
		If i >= n Then
			n = Val(*Cast(ZString Ptr,pBuf))
		Else 
			If FindDefines(pBuf,n) = 0 Then 
				n = 0
				Return 0 
			EndIf
		EndIf
	EndIf
	Return 1
End Function

Sub OperandIsORG(ByRef pStr As UByte Ptr)																				'ORG ...
'{  ORG Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:ORG地址没有写.\r\n",nline)  
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tORG\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If GetAddress(@szBuf1(0),n) Then																					'ORG addr
		curAddr = n
		AddAsmData("ORG",0,curAddr)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x\r\n",curAddr)
	#endif
#ifdef _PRINT_ERRORS
	Else
		printf(!"第%d行:地址格式错误.\r\n",nline)
#endif
	EndIf
End Sub

Sub OperandIsMOV(ByRef pStr As UByte Ptr)																				'MOV ...
'{  MOV Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:MOV第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:MOV第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tMOV\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif	
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'MOV A,..				
'{  MOV A,..
		If szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then									'MOV A,Rn			;E8 - EF
'{  MOV A,Rn
			n = szBuf2(1) - Asc("0")
			If n >= 0 AndAlso n <= 7 Then
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&HE8 + n)
			#endif
				AddAsmData("MOV",&HE8 + n,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'MOV A,@Ri			;E6 - E7
'{  MOV A,@Ri
			n = szBuf2(2) - Asc("0")
			If n = 0 OrElse n = 1 Then
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&HE6 + n)
			#endif
				AddAsmData("MOV",&HE6 + n,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'MOV A,#data		;74
'{  MOV A,#data
			If GetValue(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H74,n2)
			#endif
				AddAsmData("MOV",&H74,curAddr)
				AddAsmData("MOV",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:立即数错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'MOV A,data			;E5
'{  MOV A,data
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HE5,n2)
			#endif
				AddAsmData("MOV",&HE5,curAddr)
				AddAsmData("MOV",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:MOV第二个操作符错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
'}
	ElseIf szBuf1(0) = Asc("R") AndAlso isdigit(szBuf1(1)) AndAlso szBuf1(2) = 0 Then									'MOV Rn,..
'{  MOV Rn,..
		n = szBuf1(1) - Asc("0")
		If n >= 0 AndAlso n <= 7 Then
			If *Cast(ZString Ptr,@szBuf2(0)) = "A" Then																	'MOV Rn,A			;F8 - FF
'{  MOV Rn,A
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x\r\n",curAddr,n + &HF8)  
			#endif
				AddAsmData("MOV",n + &HF8,curAddr)
				curAddr += 1
'}
			ElseIf szBuf2(0) = Asc("#") Then																			'MOV Rn,#data		;78 - 7F
'{  MOV Rn,#data
				If GetValue(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,n + &H78,n2)
				#endif
					AddAsmData("MOV",n + &H78,curAddr)
					AddAsmData("MOV",n2,curAddr + 1)
					curAddr += 2
			#ifdef _PRINT_ERRORS
				Else
					printf(!"第%d行:立即数错误.\r\n",nline)
			#endif
				EndIf
'}
			Else																										'MOV Rn,data		;A8 - AF
'{  MOV Rn,data
				If GetAddress(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,n + &HA8,n2)
				#endif
					AddAsmData("MOV",n + &HA8,curAddr)
					AddAsmData("MOV",n2,curAddr + 1)
					curAddr += 2
			#ifdef _PRINT_ERRORS
				Else
					printf(!"第%d行:直接地址错误.\r\n",nline)
			#endif
				EndIf
'}
			EndIf
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
	#endif
		EndIf
'}
	ElseIf szBuf1(0) = Asc("@") AndAlso szBuf1(1) = Asc("R") AndAlso isdigit(szBuf1(2)) AndAlso szBuf1(3) = 0 Then		'MOV @Ri,...
'{  MOV @Ri,...
		n = szBuf1(2) - Asc("0")
		If n = 0 OrElse n = 1 Then
			If *Cast(ZString Ptr,@szBuf2(0)) = "A" Then																	'MOV @Ri,A			;F6 - F7
'{  MOV @Ri,A
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x\r\n",curAddr,n + &HF6)
			#endif	  
				AddAsmData("MOV",n + &HF6,curAddr)
				curAddr += 1
'}
			ElseIf szBuf2(0) = Asc("#") Then																			'MOV @Ri,#data		;76 - 77
'{  MOV @Ri,#data
				If GetValue(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,n + &H76,n2)  
				#endif	
					AddAsmData("MOV",n + &H76,curAddr)
					AddAsmData("MOV",n2,curAddr + 1)
					curAddr += 2
			#ifdef _PRINT_ERRORS
				Else
					printf(!"第%d行:立即数错误.\r\n",nline)
			#endif	
				EndIf
'}
			Else																										'MOV @Ri,data		;A6 - A7
'{  MOV @Ri,data
				If GetAddress(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE	
					printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,n + &HA6,n2)  
				#endif	
					AddAsmData("MOV",n + &HA6,curAddr)
					AddAsmData("MOV",n2,curAddr + 1)
					curAddr += 2
			#ifdef _PRINT_ERRORS
				Else
					printf(!"第%d行:直接地址格式错误.\r\n",nline)
			#endif	
				EndIf
'}
			EndIf
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
	#endif	
		EndIf
'}
	ElseIf *Cast(ZString Ptr,@szBuf1(0)) = "DPTR" Then																	'MOV DPTR,#data16	;90
'{  MOV DPTR,#data16
		If szBuf2(0) = Asc("#") Then
			AddAsmData("MOV",&H90,curAddr)
			If GetValue(@szBuf2(0),n2) Then
				Dim n1 As Long = Cast(UByte,(n2 Shr 8) And &HFF)
				n2 = Cast(UByte,n2 And &HFF)
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H90,n1,n2) 
			#endif 
				AddAsmData("MOV",n1,curAddr + 1)
				AddAsmData("MOV",n2,curAddr + 2)
				curAddr += 3
			Else
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H90,@szBuf2(1)) 
			#endif
				n = AddAsmData("MOV",0,curAddr + 1)
				AddAsmData("MOV",0,curAddr + 2)
				AddJmpLabels("MOV_DPTR",@szBuf2(1),curAddr,n)
				curAddr += 3 
			EndIf
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:立即数格式错误.\r\n",nline)
	#endif 
		EndIf
'}	
	ElseIf szBuf1(0) = Asc("C") Then																					'MOV C,bit			;A2
'{  MOV C,bit
		If GetAddress(@szBuf2(0),n2) Then
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HA2,n2)  
		#endif
			AddAsmData("MOV",&HA2,curAddr)
			AddAsmData("MOV",n2,curAddr + 1)
			curAddr += 2
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:位地址格式错误.\r\n",nline)
	#endif
		EndIf
'}		
	ElseIf szBuf2(0) = Asc("C") Then																					'MOV bit,C			;92
'{  MOV bit,C
		If GetAddress(@szBuf1(0),n) Then
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H92,n)  
		#endif
			AddAsmData("MOV",&H92,curAddr)
			AddAsmData("MOV",n,curAddr + 1)
			curAddr += 2
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:位地址格式错误.\r\n",nline)
	#endif
		EndIf
'}			
	Else																												'MOV data,...
'{	MOV data,...
		If *Cast(ZString Ptr,@szBuf2(0)) = "A" Then																		'MOV data,A			;F5
'{  MOV data,A
			If GetAddress(@szBuf1(0),n) Then
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HF5,n)  
			#endif
				AddAsmData("MOV",&HF5,curAddr)
				AddAsmData("MOV",n,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then								'MOV data,Rn		;88 - 8F
'{  MOV data,Rn
			n2 = szBuf2(1) - Asc("0")
			If n2 >= 0 AndAlso n2 <= 7 Then
				If GetAddress(@szBuf1(0),n) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H88 + n2,n)  
				#endif
					AddAsmData("MOV",&H88 + n2,curAddr)
					AddAsmData("MOV",n,curAddr + 1)
					curAddr += 2
			#ifdef _PRINT_ERRORS
				Else
					printf(!"第%d行:直接地址格式错误.\r\n",nline)
			#endif
				EndIf
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'MOV data,@Ri		;86 - 87
'{  MOV data,@Ri
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
				If GetAddress(@szBuf1(0),n) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H86 + n2,n)  
				#endif
					AddAsmData("MOV",&H86 + n2,curAddr)
					AddAsmData("MOV",n,curAddr + 1)
					curAddr += 2
			#ifdef _PRINT_ERRORS
				Else
					printf(!"第%d行:直接地址格式错误.\r\n",nline)
			#endif
				EndIf
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'MOV data,#data		;75
'{  MOV data,#data
			If GetAddress(@szBuf1(0),n) Then
				If GetValue(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE	
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H75,n,n2)  
				#endif
					AddAsmData("MOV",&H75,curAddr)
					AddAsmData("MOV",n,curAddr + 1)
					AddAsmData("MOV",n2,curAddr + 2)
					curAddr += 3
			#ifdef _PRINT_ERRORS
				Else
					printf(!"第%d行:立即数格式错误.\r\n",nline)
			#endif
				EndIf
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'MOV data,data		;85
'{  MOV data,data
			If GetAddress(@szBuf1(0),n) Then
				If GetAddress(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE	
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H85,n,n2)  
				#endif
					AddAsmData("MOV",&H85,curAddr)
					AddAsmData("MOV",n,curAddr + 1)
					AddAsmData("MOV",n2,curAddr + 2)
					curAddr += 3
			#ifdef _PRINT_ERRORS
				Else
					printf(!"第%d行:直接地址格式错误.\r\n",nline)
			#endif
				EndIf
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}			
		EndIf

'}
	EndIf
End Sub

Sub OperandIsMOVC(ByRef pStr As UByte Ptr)																				'MOVC ...
'{  MOVC Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:MOVC第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:MOVC第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tMOVC\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'MOVC A,...
		If *Cast(ZString Ptr,@szBuf2(0)) = "@A+DPTR" Then																'MOVC A,@A+DPTR		;93
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x\r\n",curAddr,&H93)  
		#endif
			AddAsmData("MOVC",&H93,curAddr)
			curAddr += 1
		ElseIf *Cast(ZString Ptr,@szBuf2(0)) = "@A+PC" Then																'MOVC A,@A+PC		;83
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x\r\n",curAddr,&H83)  
		#endif
			AddAsmData("MOVC",&H83,curAddr)	
			curAddr += 1
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:MOVC第二个操作符错误.\r\n",nline)
	#endif
		EndIf
#ifdef _PRINT_ERRORS
	Else
		printf(!"第%d行:MOVC第一个操作符错误.\r\n",nline)
#endif
	EndIf
End Sub

Sub OperandIsMOVX(ByRef pStr As UByte Ptr)																				'MOVX ...
'{  MOVX Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:MOVX第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:MOVX第二个操作符为空.\r\n",nline)
	EndIf
#endif	
#ifdef _PRINT_CODE
	printf(!"%3d  \tMOVX\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'MOVX A,...		
		If szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then		'MOVX A,@Ri			;E2 - E3
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&HE2 + n2)  
			#endif
				AddAsmData("MOV",&HE2 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
		ElseIf *Cast(ZString Ptr,@szBuf2(0)) = "@DPTR" Then																'MOVX A,@DPTR		;E0
		#ifdef _PRINT_OUTCODE	
			printf(!"current addr:%4x:  %2x\r\n",curAddr,&HE0)  
		#endif
			AddAsmData("MOV",&HE0,curAddr)
			curAddr += 1
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:MOVX第二个操作符错误.\r\n",nline)
	#endif
		EndIf
	ElseIf szBuf1(0) = Asc("@") AndAlso szBuf1(1) = Asc("R") AndAlso isdigit(szBuf1(2)) AndAlso szBuf1(3) = 0 Then		'MOVX @Ri,A			;F2 - F3
'{  MOVX @Ri,A
		n = szBuf1(2) - Asc("0")
		If n = 0 OrElse n = 1 Then
			If *Cast(ZString Ptr,@szBuf2(0)) = "A" Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&HF2 + n)  
			#endif
				AddAsmData("MOVX",&HF2 + n,curAddr)
				curAddr += 1	
			Else
				printf(!"第%d行:MOVX第二个操作符错误.\r\n",nline)
			EndIf
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
	#endif
		EndIf
'}
	ElseIf *Cast(ZString Ptr,@szBuf1(0)) = "@DPTR" Then																	'MOVX @DPTR,A		;F0
'{  MOVX @DPTR,A
		If *Cast(ZString Ptr,@szBuf2(0)) = "A" Then
		#ifdef _PRINT_OUTCODE	
			printf(!"current addr:%4x:  %2x\r\n",curAddr,&HF0)  
		#endif
			AddAsmData("MOVX",&HF0,curAddr)
			curAddr += 1
		Else
			printf(!"第%d行:MOVX第二个操作符错误.\r\n",nline)
		EndIf
'}
#ifdef _PRINT_ERRORS
	Else																												
		printf(!"第%d行:MOVX第一个操作符错误.\r\n",nline)
#endIf
	EndIf
End Sub

Sub OperandIsPUSH(ByRef pStr As UByte Ptr)																				'PUSH ...
'{  PUSH Get Operand 
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:PUSH第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tPUSH\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endIf
'}
	If GetAddress(@szBuf1(0),n) Then																					'PUSH data			;C0
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HC0,n)  								
	#endif
		AddAsmData("PUSH",&HC0,curAddr)
		AddAsmData("PUSH",n,curAddr)
		curAddr += 2
	Else
		printf(!"第%d行:直接地址格式错误.\r\n",nline)
	EndIf
End Sub

Sub OperandIsPOP(ByRef pStr As UByte Ptr)																				'POP ...
'{  POP Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:POP第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tPOP\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endIf
'}
	If GetAddress(@szBuf1(0),n) Then																					'POP data			;D0
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HD0,n)  								
	#endif
		AddAsmData("POP",&HD0,curAddr)
		AddAsmData("POP",n,curAddr)
		curAddr += 2
	Else
		printf(!"第%d行:直接地址格式错误.\r\n",nline)
	EndIf
End Sub

Sub OperandIsXCH(ByRef pStr As UByte Ptr)																				'XCH ...
'{  XCH Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:XCH第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:XCH第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tXCH\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'XCH A,..
		If szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then									'XCH A,Rn			;C8 - CF
'{  XCH A,Rn
			n2 = szBuf2(1) - Asc("0")
			If n2 >= 0 AndAlso n2 <= 7 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&HC8 + n2)  
			#endif
				AddAsmData("MOV",&HC8 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'XCH A,@Ri			;C6 - C7
'{  XCH A,@Ri
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&HC6 + n2)  
			#endif
				AddAsmData("XCH",&HC6 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'XCH A,data			;C5
'{  XCH A,data
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&HC5)  
			#endif
				AddAsmData("XCH",&HC5,curAddr)
				AddAsmData("XCH",n2,curAddr + 1)
				curAddr += 2	
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
	EndIf
End Sub

Sub OperandIsXCHD(ByRef pStr As UByte Ptr)																				'XCHD ...                              
'{  XCHD Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:XCHD第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:XCHD第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tXCHD\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'XCHD A,@Ri			;D6 - D7
		If szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then		'XCHD A,@Ri			;D6 - D7
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&HD6 + n2)  
			#endif
				AddAsmData("XCHD",&HD6 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf	
		EndIf
	EndIf
End Sub

Sub OperandIsANL(ByRef pStr As UByte Ptr)																				'ANL ...
'{  ANL Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:ANL第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:ANL第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tANL\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'ANL A,...
'{  ANL A,...
		If szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then									'ANL A,Rn			;58 - 5F
'{  ANL A,Rn
			n2 = szBuf2(1) - Asc("0")
			If n2 >= 0 AndAlso n2 <= 7 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H58 + n2)  
			#endif
				AddAsmData("ANL",&H58 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'ANL A,@Ri			;56 - 57
'{  ANL A,@Ri
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H56 + n2)  
			#endif
				AddAsmData("ANL",&H56 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'ANL A,#data		;54
'{  ANL A,#data
			If GetValue(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H54,n2)  
			#endif
				AddAsmData("ANL",&H54,curAddr)
				AddAsmData("ANL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'ANL A,data			;55
'{  ANL A,data	
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H55,n2)  
			#endif
				AddAsmData("ANL",&H55,curAddr)
				AddAsmData("ANL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
'}
	ElseIf *Cast(ZString Ptr,@szBuf1(0)) = "C" Then																		'ANL C,...	
'{  ANL C,...			
		If szBuf2(0) = Asc("/") Then																					'ANL C,/bit			;B0
'{  ANL C,/bit
			If GetAddress(@szBuf2(1),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HB0,n2)  
			#endif
				AddAsmData("ANL",&HB0,curAddr)
				AddAsmData("ANL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS			
			Else
				printf(!"第%d行:位地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'ANL C,bit			;82
'{  ANL C,bit
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H82,n2)  
			#endif
				AddAsmData("ANL",&H82,curAddr)
				AddAsmData("ANL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:位地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
'}
	Else																												'ANL data,...	
'{  ANL data,...	
		If *Cast(ZString Ptr,@szBuf2(0)) = "A" Then																		'ANL data,A			;52
'{  ANL data,A
			If GetAddress(@szBuf1(0),n) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H52,n)  
			#endif
				AddAsmData("ANL",&H52,curAddr)
				AddAsmData("ANL",n,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS	
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'ANL data,#data		;53
'{  ANL data,#data
			If GetAddress(@szBuf1(0),n) Then
				If GetValue(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE	
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H53,n,n2)  
				#endif
					AddAsmData("ANL",&H52,curAddr)
					AddAsmData("ANL",n,curAddr + 1)
					AddAsmData("ANL",n2,curAddr + 2)
					curAddr += 3
			#ifdef _PRINT_ERRORS	
				Else
					printf(!"第%d行:立即数格式错误.\r\n",nline)
			#endif
				EndIf
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:ANL第二个操作符错误.\r\n",nline)
	#endif
		EndIf
'}
	EndIf
End Sub

Sub OperandIsORL(ByRef pStr As UByte Ptr)																				'ORL ...
'{  ORL Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:ORL第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:ORL第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tORL\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'ORL A,...
'{  ORL A,...		
		If szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then									'ORL A,Rn			;48 - 4F
'{  ORL A,Rn
			n2 = szBuf2(1) - Asc("0")
			If n2 >= 0 AndAlso n2 <= 7 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H48 + n2)  
			#endif
				AddAsmData("ORL",&H48 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'ORL A,@Ri			;46 - 47
'{  ORL A,@Ri
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H46 + n2)  
			#endif
				AddAsmData("ORL",&H46 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'ORL A,#data		;44
'{  ORL A,#data
			If GetValue(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H44,n2)  
			#endif
				AddAsmData("ORL",&H44,curAddr)
				AddAsmData("ORL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'ORL A,data			;45
'{  ORL A,data	
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H45,n2)  
			#endif
				AddAsmData("ORL",&H45,curAddr)
				AddAsmData("ORL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
'}
	ElseIf *Cast(ZString Ptr,@szBuf1(0)) = "C" Then																		'ORL C,...
'{  ORL C,...			
		If szBuf2(0) = Asc("/") Then																					'ORL C,/bit			;A0
'{  ORL C,/bit
			If GetAddress(@szBuf2(1),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HA0,n2)  
			#endif
				AddAsmData("ORL",&HA0,curAddr)
				AddAsmData("ORL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS			
			Else
				printf(!"第%d行:位地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'ORL C,bit			;72
'{  ORL C,bit
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H72,n2)  
			#endif
				AddAsmData("ORL",&H72,curAddr)
				AddAsmData("ORL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:位地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
'}
	Else																												'ORL data,...	
'{  ORL data,...	
		If *Cast(ZString Ptr,@szBuf2(0)) = "A" Then																		'ORL data,A			;42
'{  ORL data,A
			If GetAddress(@szBuf1(0),n) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H52,n)  
			#endif
				AddAsmData("ORL",&H52,curAddr)
				AddAsmData("ORL",n,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS	
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'ORL data,#data		;43
'{  ORL data,#data
			If GetAddress(@szBuf1(0),n) Then
				If GetValue(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE	
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H53,n,n2)  
				#endif
					AddAsmData("ORL",&H52,curAddr)
					AddAsmData("ORL",n,curAddr + 1)
					AddAsmData("ORL",n2,curAddr + 2)
					curAddr += 3
			#ifdef _PRINT_ERRORS	
				Else
					printf(!"第%d行:立即数格式错误.\r\n",nline)
			#endif
				EndIf
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:ORL第二个操作符错误.\r\n",nline)
	#endif
		EndIf
'}
	EndIf
End Sub

Sub OperandIsXRL(ByRef pStr As UByte Ptr)																				'XRL ...
'{  XRL Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:XRL第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:XRL第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tXRL\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'XRL A,...
'{  XRL A,...
		If szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then									'XRL A,Rn			;68 - 6F
'{  XRL A,Rn
			n2 = szBuf2(1) - Asc("0")
			If n2 >= 0 AndAlso n2 <= 7 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H68 + n2)  
			#endif
				AddAsmData("XRL",&H68 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'XRL A,@Ri			;66 - 67
'{  XRL A,@Ri
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H66 + n2)  
			#endif
				AddAsmData("XRL",&H66 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'XRL A,#data		;64
'{  XRL A,#data
			If GetValue(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H64,n2)  
			#endif
				AddAsmData("XRL",&H64,curAddr)
				AddAsmData("XRL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'XRL A,data			;65
'{  XRL A,data
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H65,n2)  
			#endif
				AddAsmData("XRL",&H65,curAddr)
				AddAsmData("XRL",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
'}
	Else																												'XRL data,...	
'{  XRL data,...	
		If *Cast(ZString Ptr,@szBuf2(0)) = "A" Then																		'XRL data,A			;62
'{  XRL data,A
			If GetAddress(@szBuf1(0),n) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H62,n)  
			#endif
				AddAsmData("XRL",&H62,curAddr)
				AddAsmData("XRL",n,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS	
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'XRL data,#data		;63
'{  XRL data,#data
			If GetAddress(@szBuf1(0),n) Then
				If GetValue(@szBuf2(0),n2) Then
				#ifdef _PRINT_OUTCODE	
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H63,n,n2)  
				#endif
					AddAsmData("XRL",&H62,curAddr)
					AddAsmData("XRL",n,curAddr + 1)
					AddAsmData("XRL",n2,curAddr + 2)
					curAddr += 3
			#ifdef _PRINT_ERRORS	
				Else
					printf(!"第%d行:立即数格式错误.\r\n",nline)
			#endif
				EndIf
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:XRL第二个操作符错误.\r\n",nline)
	#endif
		EndIf
'}
	EndIf
End Sub

Sub OperandIsSETB(ByRef pStr As UByte Ptr)																				'SETB ...
'{  SETB Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:SETB第一个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tSETB\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "C" Then																			'SETB C				;D3
	#ifdef _PRINT_OUTCODE	
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HD3)  									'SETB C				;D3
	#endif
		AddAsmData("XRL",&HD3,curAddr)
		curAddr += 1	
	Else																												'SETB bit			;D2 bit
		If GetAddress(@szBuf1(0),n) Then
		#ifdef _PRINT_OUTCODE	
			printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HD2,n)  							'SETB bit			;D2
		#endif
			AddAsmData("XRL",&HD2,curAddr)
			AddAsmData("XRL",n,curAddr)
			curAddr += 2
	#ifdef _PRINT_ERRORS	
		Else
			printf(!"第%d行:位地址格式错误.\r\n",nline)
	#endif
		EndIf
	EndIf
End Sub

Sub OperandIsCLR(ByRef pStr As UByte Ptr)																				'CLR ...
'{  CLR Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS	
	If n = 0 Then
		printf("第%d行:CLR第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tCLR\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}	
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'CLR A				;E4
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HE4) 
	#endIf 
		AddAsmData("CLR",&HE4,curAddr)
		curAddr += 1
	ElseIf *Cast(ZString Ptr,@szBuf1(0)) = "C" Then																		'CLR C				;C3
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HC3) 
	#endif
		AddAsmData("CLR",&HC3,curAddr)
		curAddr += 1
	Else																												'CLR bit			;C2 bit
		If GetAddress(@szBuf1(0),n) Then																				'
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HC2,n) 
		#endIf
			AddAsmData("CLR",&HC2,curAddr)
			AddAsmData("CLR",n,curAddr + 1)
			curAddr += 2	
	#ifdef _PRINT_ERRORS	
		Else
			printf(!"第%d行:CLR操作寄存器错误.\r\n",nline)
	#endif
		EndIf
	EndIf
End Sub

Sub OperandIsCPL(ByRef pStr As UByte Ptr)																				'CPL ...
'{  CPL Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:CPL第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tCPL\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endIf
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'CPL A				;F4
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HF4) 
	#endIf 
		AddAsmData("CPL",&HF4,curAddr)
		curAddr += 1	
	ElseIf *Cast(ZString Ptr,@szBuf1(0)) = "C" Then																		'CPL C				;B3
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HB3) 
	#endIf
		AddAsmData("CPL",&HB3,curAddr)
		curAddr += 1	
	Else																												'CPL bit			;B2 bit
		If GetAddress(@szBuf1(0),n) Then
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HB2,n) 
		#endIf
			AddAsmData("CPL",&HB2,curAddr)
			AddAsmData("CPL",n,curAddr)
			curAddr += 2
	#ifdef _PRINT_ERRORS	
		Else
			printf(!"第%d行:位地址格式错误.\r\n",nline)
	#endif
		EndIf
	EndIf
End Sub

Sub OperandIsRL(ByRef pStr As UByte Ptr)																				'RL ...
'{  RL Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS	
	If n = 0 Then
		printf("第%d行:RL第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tRL\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'RL A				;23
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&H23) 
	#endIf 
		AddAsmData("RL",&H23,curAddr)
		curAddr += 1	
#ifdef _PRINT_ERRORS
	Else
		printf("第%d行:RL第一个操作符错误.\r\n",nline)
#endif
	EndIf
End Sub

Sub OperandIsRLC(ByRef pStr As UByte Ptr)																				'RLC ...
'{  RLC Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:RLC第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tRLC\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endIf
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'RLC A				;33
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&H33) 
	#endIf 
		AddAsmData("RLC",&H33,curAddr)
		curAddr += 1	
#ifdef _PRINT_ERRORS
	Else
		printf("第%d行:RLC第一个操作符错误.\r\n",nline)
#endif
	EndIf
End Sub

Sub OperandIsRR(ByRef pStr As UByte Ptr)																				'RR ...
'{  RR Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:RR第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tRR\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'RR A				;03
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&H03) 
	#endIf 
		AddAsmData("RR",&H03,curAddr)
		curAddr += 1	
#ifdef _PRINT_ERRORS
	Else
		printf("第%d行:RR第一个操作符错误.\r\n",nline)
#endif
	EndIf
End Sub

Sub OperandIsRRC(ByRef pStr As UByte Ptr)																				'RRC ...
'{  RRC Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:RRC第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tRRC\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endIf
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'RRC A				;13
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&H13) 
	#endIf 
		AddAsmData("RRC",&H13,curAddr)
		curAddr += 1	
#ifdef _PRINT_ERRORS
	Else
		printf("第%d行:RR第一个操作符错误.\r\n",nline)
#endif
	EndIf
End Sub

Sub OperandIsSWAP(ByRef pStr As UByte Ptr)																				'SWAP ...
'{  SWAP Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:SWAP第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tSWAP\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endIf
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'SWAP A				;C4
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HC4) 
	#endIf 
		AddAsmData("SWAP",&HC4,curAddr)
		curAddr += 1	
#ifdef _PRINT_ERRORS
	Else
		printf("第%d行:RR第一个操作符错误.\r\n",nline)
#endif
	EndIf
End Sub

Sub OperandIsADD(ByRef pStr As UByte Ptr)																				'ADD ...
'{  ADD Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:ADD第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:ADD第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tADD\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'ADD A,...
		If szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then									'ADD A,Rn			;28 - 2F
'{  ADD A,Rn
			n2 = szBuf2(1) - Asc("0")
			If n2 >= 0 AndAlso n2 <= 7 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H28 + n2)  
			#endif
				AddAsmData("ADD",&H28 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'ADD A,@Ri			;26 - 27
'{  ADD A,@Ri
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H26 + n2)  
			#endif
				AddAsmData("ADD",&H26 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'ADD A,#data		;24
'{  ADD A,#data
			If GetValue(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H24,n2)  
			#endif
				AddAsmData("ADD",&H24,curAddr)
				AddAsmData("ADD",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'ADD A,data			;25
'{  ADD A,data
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H25,n2)  
			#endif
				AddAsmData("ADD",&H25,curAddr)
				AddAsmData("ADD",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
	EndIf
End Sub

Sub OperandIsADDC(ByRef pStr As UByte Ptr)																				'ADDC ...
'{  ADDC Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:ADDC第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:ADDC第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tADDC\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'ADDC A,... 
		If szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then									'ADDC A,Rn			;38 - 3F
'{ 	ADDC A,Rn
			n2 = szBuf2(1) - Asc("0")
			If n2 >= 0 AndAlso n2 <= 7 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H38 + n2)  
			#endif
				AddAsmData("ADDC",&H38 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'ADDC A,@Ri			;36 - 37
'{  ADDC A,@Ri
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H36 + n2)  
			#endif
				AddAsmData("ADDC",&H36 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'ADDC A,#data		;34
'{  ADDC A,#data
			If GetValue(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H34,n2)  
			#endif
				AddAsmData("ADDC",&H34,curAddr)
				AddAsmData("ADDC",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'ADDC A,data		;35
'{  ADDC A,data
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H35,n2)  
			#endif
				AddAsmData("ADDC",&H35,curAddr)
				AddAsmData("ADDC",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
	EndIf
End Sub

Sub OperandIsSUBB(ByRef pStr As UByte Ptr)																				'SUBB ...
'{  SUBB Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:SUBB第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
#ifdef _PRINT_ERRORS
	n2 = GetUperWordToBuf(pStr,szBuf2())
	If n2 = 0 Then
		printf(!"第%d行:SUBB第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tSUBB\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'SUBB A,...
		If szBuf2(0) = Asc("R") AndAlso isdigit(szBuf2(1)) AndAlso szBuf2(2) = 0 Then									'SUBB A,Rn			;98 - 9F
'{  SUBB A,Rn
			n2 = szBuf2(1) - Asc("0")
			If n2 >= 0 AndAlso n2 <= 7 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H98 + n2)  
			#endif
				AddAsmData("SUBB",&H98 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("@") AndAlso szBuf2(1) = Asc("R") AndAlso isdigit(szBuf2(2)) AndAlso szBuf2(3) = 0 Then	'SUBB A,@Ri			;96 - 97
'{  SUBB A,@Ri
			n2 = szBuf2(2) - Asc("0")
			If n2 = 0 OrElse n2 = 1 Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x\r\n",curAddr,&H96 + n2)  
			#endif
				AddAsmData("SUBB",&H96 + n2,curAddr)
				curAddr += 1
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
		#endif
			EndIf
'}
		ElseIf szBuf2(0) = Asc("#") Then																				'SUBB A,#data		;94
'{  SUBB A,#data
			If GetValue(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H94,n2)  
			#endif
				AddAsmData("SUBB",&H94,curAddr)
				AddAsmData("SUBB",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'SUBB A,data		;95
'{  SUBB A,data
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE	
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H95,n2)  
			#endif
				AddAsmData("SUBB",&H95,curAddr)
				AddAsmData("SUBB",n2,curAddr + 1)
				curAddr += 2
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
	EndIf
End Sub

Sub OperandIsINC(ByRef pStr As UByte Ptr)																				'INC ...
'{  INC Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS	
	If n = 0 Then
		printf(!"第%d行:INC第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tINC\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'INC A				;04
'{  INC A
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&H04)  
	#endif
		AddAsmData("INC",&H04,curAddr)
		curAddr += 1
'}
	ElseIf szBuf1(0) = Asc("R") AndAlso isdigit(szBuf1(1)) AndAlso szBuf1(2) = 0 Then									'INC Rn				;08 - 0F
'{  INC Rn
		n = szBuf1(1) - Asc("0")
		If n >= 0 AndAlso n <= 7 Then
		#ifdef _PRINT_OUTCODE	
			printf(!"current addr:%4x:  %2x\r\n",curAddr,&H08 + n)  
		#endif
			AddAsmData("ANL",&H08 + n,curAddr)
			curAddr += 1
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
	#endif
		EndIf
'}
	ElseIf szBuf1(0) = Asc("@") AndAlso szBuf1(1) = Asc("R") AndAlso isdigit(szBuf1(2)) AndAlso szBuf1(3) = 0 Then		'INC @Ri			;06 - 07
'{  INC @Ri
		n = szBuf1(2) - Asc("0")
		If n = 0 OrElse n = 1 Then
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x\r\n",curAddr,n + &H06) 
		#endIf	
			AddAsmData("INC",n + &H06,curAddr)
			curAddr += 1
	#ifdef _PRINT_ERRORS		
		Else
			printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
	#endif
		EndIf
'}
	ElseIf *Cast(ZString Ptr,@szBuf1(0)) = "DPTR" Then																	'INC DPTR			;A3
'{  INC DPTR
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HA3) 
	#endIf
		AddAsmData("INC",&HA3,curAddr)
		curAddr += 1
'}
	Else																												'INC data			;05 data
'{  INC data
		If GetAddress(@szBuf1(0),n) Then
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H05,n) 
		#endIf	
			AddAsmData("INC",&H05,curAddr)
			AddAsmData("INC",n,curAddr + 1)
			curAddr += 2
	#ifdef _PRINT_ERRORS			
		Else
			printf("第%d行:INC操作寄存器错误.\r\n",nline)
	#endif		
		EndIf
'}
	EndIf
End Sub

Sub OperandIsDEC(ByRef pStr As UByte Ptr)																				'DEC ...
'{  DEC Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:DEC第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tDEC\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endIf
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'DEC A				;14
'{  INC A
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&H04)  
	#endif
		AddAsmData("INC",&H14,curAddr)
		curAddr += 1
'}
	ElseIf szBuf1(0) = Asc("R") AndAlso isdigit(szBuf1(1)) AndAlso szBuf1(2) = 0 Then									'DEC Rn				;18 - 1F
'{  INC Rn
		n = szBuf1(1) - Asc("0")
		If n >= 0 AndAlso n <= 7 Then
		#ifdef _PRINT_OUTCODE	
			printf(!"current addr:%4x:  %2x\r\n",curAddr,&H18 + n)  
		#endif
			AddAsmData("ANL",&H18 + n,curAddr)
			curAddr += 1
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
	#endif
		EndIf
'}
	ElseIf szBuf1(0) = Asc("@") AndAlso szBuf1(1) = Asc("R") AndAlso isdigit(szBuf1(2)) AndAlso szBuf1(3) = 0 Then		'DEC @Ri			;16 - 17
'{  INC @Ri
		n = szBuf1(2) - Asc("0")
		If n = 0 OrElse n = 1 Then
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x\r\n",curAddr,n + &H16) 
		#endIf	
			AddAsmData("INC",n + &H16,curAddr)
			curAddr += 1
	#ifdef _PRINT_ERRORS		
		Else
			printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
	#endif
		EndIf
'}
	Else																												'DEC data			;15 data
'{  INC data
		If GetAddress(@szBuf1(0),n) Then
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x\r\n",curAddr,&H15) 
		#endIf	
			AddAsmData("INC",&H15,curAddr)
			AddAsmData("INC",n,curAddr + 1)
			curAddr += 2
	#ifdef _PRINT_ERRORS			
		Else
			printf("第%d行:INC操作寄存器错误.\r\n",nline)
	#endif		
		EndIf
'}
	EndIf	
End Sub

Sub OperandIsMUL(ByRef pStr As UByte Ptr)																				'MUL ...
'{  MUL Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:MUL第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tMUL\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If szBuf1(0) = Asc("A") AndAlso szBuf1(1) = Asc("B") AndAlso szBuf1(2) = 0 Then										'MUL AB				;A4					
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HA4) '
	#endif
		AddAsmData("MUL",&HA4,curAddr)
		curAddr += 1
#ifdef _PRINT_ERRORS	
	Else
		printf(!"第%d行:MUL操作符错误.\r\n",nline)
#endIf				
	EndIf
End Sub

Sub OperandIsDIV(ByRef pStr As UByte Ptr)																				'DIV ...
'{  DIV Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:DIV第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tDIV\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If szBuf1(0) = Asc("A") AndAlso szBuf1(1) = Asc("B") AndAlso szBuf1(2) = 0 Then										'DIV AB				;84
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&H84) 
	#endif
		AddAsmData("DIV",&H84,curAddr)
		curAddr += 1
#ifdef _PRINT_ERRORS	
	Else
		printf(!"第%d行:DIV操作符错误.\r\n",nline)
#endIf				
	EndIf
End Sub

Sub OperandIsDA(ByRef pStr As UByte Ptr)																				'DA ...
'{  DA Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:DA第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tDA\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'DA A				;D4
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&HD4) 
	#endif
		AddAsmData("DA",&HD4,curAddr)
		curAddr += 1
#ifdef _PRINT_ERRORS	
	Else
		printf(!"第%d行:DA操作符错误.\r\n",nline)
#endIf		
	EndIf
End Sub

Sub OperandIsAJMP(ByRef pStr As UByte Ptr)																				'AJMP ...
'{  AJMP Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:AJMP第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tAJMP\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'AJMP Addr11		;aaa0 0001,aaaa aaaa
	If GetAddress(@szBuf1(0),n) Then
		Dim n1 As Long = &H01 Or Cast(UByte,(n Shr 8) And &H03)
		Dim n2 As Long = Cast(UByte,n And &HFF)
		AddAsmData("AJMP",n1,curAddr)
		AddAsmData("AJMP",n2,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,n1,n2)
	#endif
	Else
		n = AddAsmData("AJMP",&H01,curAddr)
		AddAsmData("AJMP",&H00,curAddr + 1)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H01,@szBuf1(0)) ';aaa0 0001,aaaa aaaa
	#endif
		AddJmpLabels("AJMP",@szBuf1(0),curAddr,n)
	EndIf
	curAddr += 2
'}
End Sub

Sub OperandIsLJMP(ByRef pStr As UByte Ptr)																				'LJMP ...
'{  LJMP Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:LJMP第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tLJMP\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'LJMP Addr16		;02 Hi Lo	
	AddAsmData("LJMP",&H02,curAddr)
	If GetAddress(@szBuf1(0),n) Then
		Dim n1 As Long = Cast(UByte,(n Shr 8) And &HFF)
		Dim n2 As Long = Cast(UByte,n And &HFF)
		AddAsmData("LJMP",n1,curAddr + 1)
		AddAsmData("LJMP",n2,curAddr + 2)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H02,n1,n2) '02 Hi Lo
	#endif
	Else
		n = AddAsmData("LJMP",&H00,curAddr + 1)
		AddAsmData("LJMP",&H00,curAddr + 2)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H02,@szBuf1(0)) '02 Hi Lo
	#endif
		AddJmpLabels("LJMP",@szBuf1(0),curAddr,n)		
	EndIf
	curAddr += 3
'}	
End Sub

Sub OperandIsSJMP(ByRef pStr As UByte Ptr)																				'SJMP ...
'{  SJMP Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:SJMP第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tSJMP\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endIf
'}
'{  Set Data																											'SJMP rel			;80 rel
	AddAsmData("SJMP",&H80,curAddr)
	If *Cast(ZString Ptr,@szBuf1(0)) = "$" Then
		n = Cast(UByte,-2)
		AddAsmData("SJMP",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H80,n) 						'
	#endif		
	ElseIf GetAddress(@szBuf1(0),n) Then
		AddAsmData("SJMP",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H80,n) 						'
	#endif		
	Else
		Dim t As Long = AddAsmData("SJMP",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H80,@szBuf1(0)) 						'
	#endif
		AddJmpLabels("SJMP",@szBuf1(0),curAddr,t)	
	EndIf
	curAddr += 2
'}
End Sub

Sub OperandIsJMP(ByRef pStr As UByte Ptr)																				'JMP ...
'{  JMP Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:JMP第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tJMP\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
	If *Cast(ZString Ptr,@szBuf1(0)) = "@A+DPTR" Then 																	'JMP @A+DPTR		;73
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x\r\n",curAddr,&H73) 	
	#endif
		AddAsmData("JMP",&H73,curAddr)
		curAddr += 1	
	Else																												'JMP rel			;80 rel
		AddAsmData("JMP",&H80,curAddr)
		If *Cast(ZString Ptr,@szBuf1(0)) = "$" Then
			n = Cast(UByte,-2)
			AddAsmData("JMP",n,curAddr + 1)
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H80,n) 						'
		#endif	
		ElseIf GetAddress(@szBuf1(0),n) Then
			AddAsmData("JMP",n,curAddr + 1)
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H80,n) 						'
		#endif	
		Else
			Dim t As Long = AddAsmData("JMP",n,curAddr + 1)
		#ifdef _PRINT_OUTCODE 
			printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H80,@szBuf1(0)) 						'
		#endif
			AddJmpLabels("JMP",@szBuf1(0),curAddr,t)		
		EndIf
		curAddr += 2
	EndIf
End Sub

Sub OperandIsJZ(ByRef pStr As UByte Ptr)																				'JZ ...
'{  JZ Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:JZ第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tJZ\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'JZ rel				;60 rel
	AddAsmData("JZ",&H60,curAddr)
	If *Cast(ZString Ptr,@szBuf1(0)) = "$" Then
		n = Cast(UByte,-2)
		AddAsmData("JZ",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H60,n) 						'
	#endif
	ElseIf GetAddress(@szBuf1(0),n) Then
		AddAsmData("JZ",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H60,n) 						'
	#endif
	Else
		Dim t As Long = AddAsmData("JZ",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H60,@szBuf1(0)) 						'
	#endif
		AddJmpLabels("JZ",@szBuf1(0),curAddr,t)		
	EndIf
	curAddr += 2
'}
End Sub

Sub OperandIsJNZ(ByRef pStr As UByte Ptr)																				'JNZ ...
'{  JNZ Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:JNZ第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tJNZ\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'JNZ rel			;70 rel
	AddAsmData("JNZ",&H70,curAddr)
	If *Cast(ZString Ptr,@szBuf1(0)) = "$" Then
		n = Cast(UByte,-2)
		AddAsmData("JNZ",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H70,n) 						'
	#endif	
	ElseIf GetAddress(@szBuf1(0),n) Then
		AddAsmData("JNZ",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H70,n) 						'
	#endif	
	Else
		Dim t As Long = AddAsmData("JNZ",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H70,@szBuf1(0)) 						'
	#endif
		AddJmpLabels("JNZ",@szBuf1(0),curAddr,t)	
	EndIf
	curAddr += 2
'}
End Sub

Sub OperandIsJC(ByRef pStr As UByte Ptr)																				'JC ...
'{  JC Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:JC第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tJC\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'JC rel				;40 rel	
	AddAsmData("JC",&H40,curAddr)
	If *Cast(ZString Ptr,@szBuf1(0)) = "$" Then
		n = Cast(UByte,-2)
		AddAsmData("JC",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H40,n) 						
	#endif
	ElseIf GetAddress(@szBuf1(0),n) Then
		AddAsmData("JC",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H40,n) 						
	#endif
	Else
		Dim t As Long = AddAsmData("JC",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H40,@szBuf1(0)) 						
	#endif
		AddJmpLabels("JC",@szBuf1(0),curAddr,t)		
	EndIf
	curAddr += 2
'}
End Sub

Sub OperandIsJNC(ByRef pStr As UByte Ptr)																				'JNC ...
'{  JNC Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:JNC第一个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE	
	printf(!"%3d  \tJNC\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'JNC rel			;50 rel	
	AddAsmData("JNC",&H50,curAddr)
	If *Cast(ZString Ptr,@szBuf1(0)) = "$" Then
		n = Cast(UByte,-2)
		AddAsmData("JNC",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H50,n) 						'
	#endif	
	ElseIf GetAddress(@szBuf1(0),n) Then
		AddAsmData("JNC",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&H50,n) 						'
	#endif	
	Else
		Dim t As Long = AddAsmData("JNC",n,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H50,@szBuf1(0)) 						'
	#endif
		AddJmpLabels("JNC",@szBuf1(0),curAddr,t)	
	EndIf
	curAddr += 2
'}
End Sub

Sub OperandIsJB(ByRef pStr As UByte Ptr)																				'JB ...
'{  JB Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:JB第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:JB第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tJB\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'JB bit,rel			;20 rel
	If GetAddress(@szBuf1(0),n) Then															
		AddAsmData("JB",&H20,curAddr)
		AddAsmData("JB",n,curAddr + 1)
		If *Cast(ZString Ptr,@szBuf2(0)) = "$" Then
			n2 = Cast(UByte,-3)
			AddAsmData("JB",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H20,n,n2) 						'
		#endif	
		ElseIf GetAddress(@szBuf2(0),n2) Then
			AddAsmData("JB",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H20,n,n2) 						'
		#endif	
		Else
			Dim t As Long = AddAsmData("JB",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE 
			printf(!"current addr:%4x:  %2x,%2x,%s\r\n",curAddr,&H20,n,@szBuf2(0)) 						'
		#endif
			AddJmpLabels("JB",@szBuf2(0),curAddr,t)	
		EndIf
		curAddr += 3
#ifdef _PRINT_ERRORS
	Else
		printf(!"第%d行:位地址格式错误.\r\n",nline)
#endif
	EndIf
'}
End Sub

Sub OperandIsJNB(ByRef pStr As UByte Ptr)																				'JNB ...
'{  JNB Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte	
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS	
	If n = 0 Then
		printf(!"第%d行:JNB第一个操作符为空.\r\n",nline)
	EndIf
#endif	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:JNB第二个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tJNB\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'JNB bit,rel		;30 rel
	If GetAddress(@szBuf1(0),n) Then																			
		AddAsmData("JNB",&H30,curAddr)
		AddAsmData("JNB",n,curAddr + 1)
		If *Cast(ZString Ptr,@szBuf2(0)) = "$" Then
			n2 = Cast(UByte,-3)
			AddAsmData("JNB",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE	
			printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H30,n,n2) 
		#endif
		ElseIf GetAddress(@szBuf2(0),n2) Then
			AddAsmData("JNB",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE	
			printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H30,n,n2) 
		#endif
		Else
			Dim t As Long = AddAsmData("JNB",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE 
			printf(!"current addr:%4x:  %2x,%2x,%s\r\n",curAddr,&H30,n,@szBuf2(0)) 						'
		#endif
			AddJmpLabels("JNB",@szBuf2(0),curAddr,t)		
		EndIf
		curAddr += 3
#ifdef _PRINT_ERRORS
	Else
		printf(!"第%d行:位地址格式错误.\r\n",nline)
#endif
	EndIf
'}
End Sub

Sub OperandIsJBC(ByRef pStr As UByte Ptr)																				'JBC ...
'{  JBC Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS	
	If n = 0 Then
		printf(!"第%d行:JBC第一个操作符为空.\r\n",nline)
	EndIf
#endIf	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf(!"第%d行:JBC第二个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tJBC\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'JBC bit,rel		;10 rel
	If GetAddress(@szBuf1(0),n) Then
		AddAsmData("JBC",&H10,curAddr)
		AddAsmData("JBC",n,curAddr + 1)
		If *Cast(ZString Ptr,@szBuf2(0)) = "$" Then
			n2 = Cast(UByte,-3)
			AddAsmData("JBC",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H10,n,n2) 
		#endif
		ElseIf GetAddress(@szBuf2(0),n2) Then
			AddAsmData("JBC",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE
			printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H10,n,n2) 
		#endif
		Else
			Dim t As Long = AddAsmData("JBC",n2,curAddr + 2)
		#ifdef _PRINT_OUTCODE 
			printf(!"current addr:%4x:  %2x,%2x,%s\r\n",curAddr,&H10,n,@szBuf2(0)) 						'
		#endif
			AddJmpLabels("JBC",@szBuf2(0),curAddr,t)	
		EndIf
		curAddr += 3
#ifdef _PRINT_ERRORS
	Else
		printf(!"第%d行:位地址格式错误.\r\n",nline)
#endif
	EndIf
'}
End Sub

Sub OperandIsACALL(ByRef pStr As UByte Ptr)																				'ACALL ...
'{  ACALL Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:ACALL第一个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tACALL\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'ACALL Addr11		;aaa1 0001,aaaa aaaa
	If GetAddress(@szBuf1(0),n) Then
		Dim n1 As Long = &H11 Or Cast(UByte,(n Shr 8) And &H03)
		Dim n2 As Long = Cast(UByte,n And &HFF)
		AddAsmData("ACALL",n1,curAddr)
		AddAsmData("ACALL",n2,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,n1,n2)
	#endif
	Else
		n = AddAsmData("ACALL",&H11,curAddr)
		AddAsmData("ACALL",&H00,curAddr + 1)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H11,@szBuf1(0))
	#endif
		AddJmpLabels("ACALL",@szBuf1(0),curAddr,n)	
	EndIf
	curAddr += 2	
'}
End Sub

Sub OperandIsLCALL(ByRef pStr As UByte Ptr)																				'LCALL ...
'{  LCALL Get Operand
	Dim n As Long
	Dim szBuf1(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:LCALL第一个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tLCALL\t" & *Cast(ZString Ptr,@szBuf1(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'LCALL Addr16		;12,Hi Lo
	AddAsmData("LCALL",&H12,curAddr)
	If GetAddress(@szBuf1(0),n) Then
		Dim n1 As Long = Cast(UByte,(n Shr 8) And &HFF)
		Dim n2 As Long = Cast(UByte,n And &HFF)
		AddAsmData("LCALL",n1,curAddr + 1)
		AddAsmData("LCALL",n2,curAddr + 2)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&H12,n1,n2) 						
	#endif
	Else
		n = AddAsmData("LCALL",&H00,curAddr + 1)
		AddAsmData("LCALL",&H00,curAddr + 2)
	#ifdef _PRINT_OUTCODE 
		printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&H12,@szBuf1(0)) 						
	#endif
		AddJmpLabels("LCALL",@szBuf1(0),curAddr,n)
	EndIf
	curAddr += 3
'}
End Sub

Sub OperandIsCJNE(ByRef pStr As UByte Ptr)																				'CJNE ...
'{  CJNE Get Operand
	Dim n As Long,n2 As Long,n3 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte,szBuf3(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf("第%d行:CJNE第一个操作符为空.\r\n",nline)
	EndIf
#endif
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS
	If n2 = 0 Then
		printf("第%d行:CJNE第二个操作符为空.\r\n",nline)
	EndIf
#endif
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n3 = GetUperWordToBuf(pStr,szBuf3())
#ifdef _PRINT_ERRORS
	If n3 = 0 Then
		printf("第%d行:CJNE第三个操作符为空.\r\n",nline)
	EndIf
#endif
#ifdef _PRINT_CODE
	printf(!"%3d  \tCJNE\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & "," & *Cast(ZString Ptr,@szBuf3(0)) & Chr(13,10),nline)
#endif
'}	
	If *Cast(ZString Ptr,@szBuf1(0)) = "A" Then																			'CJNE A,...
'{  CJNE A,...
		If szBuf2(0) = Asc("#") Then																					'CJNE A,#data,rel	;B4
'{  CJNE A,#data,rel
			If GetValue(@szBuf2(0),n2) Then
				AddAsmData("CJNE",&HB4,curAddr)
				AddAsmData("CJNE",n2,curAddr + 1)
				If GetAddress(@szBuf3(0),n3) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HB4,n2,n3) 
				#endif
					AddAsmData("CJNE",n3,curAddr + 2)
				Else
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HB4,n2,0) 
				#endif
					n = AddAsmData("CJNE",0,curAddr + 2)
					AddJmpLabels("CJNE",@szBuf3(0),curAddr,n)		
				EndIf
				curAddr += 3
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
'}
		Else																											'CJNE A,data,rel	;B5
'{  CJNE A,data,rel
			If GetAddress(@szBuf2(0),n2) Then
				AddAsmData("CJNE",&HB5,curAddr)
				AddAsmData("CJNE",n2,curAddr + 1)
				If GetAddress(@szBuf3(0),n3) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HB5,n2,n3) 
				#endif
					AddAsmData("CJNE",n3,curAddr + 2)
				Else
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HB5,n2,0) 
				#endif
					n = AddAsmData("CJNE",0,curAddr + 2)
					AddJmpLabels("CJNE",@szBuf3(0),curAddr,n)		
				EndIf
				curAddr += 3
		#ifdef _PRINT_ERRORS		
			Else
				printf(!"第%d行:直接地址格式错误.\r\n",nline)
		#endif
			EndIf
'}
		EndIf
'}
	ElseIf szBuf1(0) = Asc("R") AndAlso isdigit(szBuf1(1)) AndAlso szBuf1(2) = 0 Then									'CJNE Rn,#data,rel	;B8 - BF
'{  CJNE Rn,#data,rel
		n = szBuf1(1) - Asc("0")
		If n >= 0 AndAlso n <= 7 Then
			If GetValue(@szBuf2(0),n2) Then
				AddAsmData("CJNE",&HB8 + n,curAddr)
				AddAsmData("CJNE",n2,curAddr + 1)
				If GetAddress(@szBuf3(0),n3) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HB8 + n,n2,n3) 
				#endif
					AddAsmData("CJNE",n3,curAddr + 2)	
				Else
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HB8 + n,n2,0) 
				#endif
					n = AddAsmData("CJNE",0,curAddr + 2)
					AddJmpLabels("CJNE",@szBuf3(0),curAddr,n)		
				EndIf
				curAddr += 3
		#ifdef _PRINT_ERRORS	
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
	#endif
		EndIf
'}
	ElseIf szBuf1(0) = Asc("@") AndAlso szBuf1(1) = Asc("R") AndAlso isdigit(szBuf1(2)) AndAlso szBuf1(3) = 0 Then		'CJNE @Ri,#data,rel	;B6 - B7
'{  CJNE @Ri,#data,rel
		n = szBuf1(2) - Asc("0")
		If n = 0 OrElse n = 1 Then
			If GetValue(@szBuf2(0),n2) Then
				AddAsmData("CJNE",&HB8 + n,curAddr)
				AddAsmData("CJNE",n2,curAddr + 1)
				If GetAddress(@szBuf3(0),n3) Then
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HB6 + n,n2,n3) 
				#endif
					AddAsmData("CJNE",n3,curAddr + 2)	
				Else
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HB6 + n,n2,0) 
				#endif
					n = AddAsmData("CJNE",0,curAddr + 2)
					AddJmpLabels("CJNE",@szBuf3(0),curAddr,n)		
				EndIf		
				curAddr += 3
		#ifdef _PRINT_ERRORS	
			Else
				printf(!"第%d行:立即数格式错误.\r\n",nline)
		#endif
			EndIf
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:寄存器@R超出0-1范围.\r\n",nline)
	#endif
		EndIf
'}
#ifdef _PRINT_ERRORS
	Else									
		printf("第%d行:CJNE第一个操作符错误.\r\n",nline)
#endif
	EndIf
End Sub

Sub OperandIsDJNZ(ByRef pStr As UByte Ptr)																				'DJNZ ...
'{  DJNZ Get Operand
	Dim n As Long,n2 As Long
	Dim szBuf1(Any) As UByte,szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf1())
#ifdef _PRINT_ERRORS
	If n = 0 Then
		printf(!"第%d行:DJNZ第一个操作符为空.\r\n",nline)
	EndIf
#endIf	
	SkipSpace(pStr)
	If *pStr = Asc(",") Then pStr += 1
	n2 = GetUperWordToBuf(pStr,szBuf2())
#ifdef _PRINT_ERRORS	
	If n2 = 0 Then
		printf(!"第%d行:DJNZ第二个操作符为空.\r\n",nline)
	EndIf
#endIf
#ifdef _PRINT_CODE
	printf(!"%3d  \tDJNZ\t" & *Cast(ZString Ptr,@szBuf1(0)) & "," & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
'}
'{  Set Data																											'DJNZ Rn,rel		;D8 - DF,rel
	If szBuf1(0) = Asc("R") AndAlso isdigit(szBuf1(1)) AndAlso szBuf1(2) = 0 Then
		n = szBuf1(1) - Asc("0")
		If n >= 0 AndAlso n <= 7 Then
			AddAsmData("DJNZ",&HD8 + n,curAddr)
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%2x\r\n",curAddr,&HD8 + n,n2) 
			#endif	
				AddAsmData("DJNZ",n2,curAddr + 1)
			Else
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%s\r\n",curAddr,&HD8 + n,@szBuf2(0)) 
			#endif	
				n = AddAsmData("DJNZ",0,curAddr + 1)
				AddJmpLabels("DJNZ",@szBuf2(0),curAddr,n)
			EndIf
			curAddr += 2
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:寄存器R超出0-7范围.\r\n",nline)
	#endif
		EndIf
	Else																												'DJNZ data,rel		;D5,rel
		If GetAddress(@szBuf1(0),n) Then
			AddAsmData("DJNZ",&HD5,curAddr)
			AddAsmData("DJNZ",n,curAddr + 1)
			If GetAddress(@szBuf2(0),n2) Then
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",curAddr,&HD5,n,n2) 
			#endif	
				AddAsmData("DJNZ",0,curAddr + 2)
			Else
			#ifdef _PRINT_OUTCODE
				printf(!"current addr:%4x:  %2x,%2x,%s\r\n",curAddr,&HD5,n,@szBuf2(0)) 
			#endif		
				n = AddAsmData("DJNZ",0,curAddr + 2)
				AddJmpLabels("DJNZ",@szBuf2(0),curAddr,n)
			EndIf
			curAddr += 3
	#ifdef _PRINT_ERRORS
		Else
			printf(!"第%d行:直接地址格式错误.\r\n",nline)
	#endif
		EndIf
	EndIf
'}
End Sub

Sub OperandIsRET()																										'RET 				;22
#ifdef _PRINT_CODE
	printf(!"%3d  \tRET\r\n",nline)
#endif
#ifdef _PRINT_OUTCODE
	printf(!"current addr:%4x:  %2x\r\n",curAddr,&H22) 
#endif
	AddAsmData("RET",&H22,curAddr)
	curAddr += 1
End Sub

Sub OperandIsRETI()																										'RETI				;32
#ifdef _PRINT_CODE
	printf(!"%3d  \tRETI\r\n",nline)
#endif
#ifdef _PRINT_OUTCODE
	printf(!"current addr:%4x:  %2x\r\n",curAddr,&H32) 
#endif
	AddAsmData("RETI",&H32,curAddr)
	curAddr += 1
End Sub

Sub OperandIsNOP()																										'NOP				;00
#ifdef _PRINT_CODE
	printf(!"%3d  \tNOP\r\n",nline)
#endif
#ifdef _PRINT_OUTCODE
	printf(!"current addr:%4x:  %2x\r\n",curAddr,&H00) 
#endif
	AddAsmData("NOP",&H00,curAddr)
	curAddr += 1
End Sub

Sub OperandIsDB(ByRef pStr As ZString Ptr)																				'DB					;...
	Dim n As Long
#ifdef _PRINT_CODE
	printf(!"%3d  \tDB\t",nline)
#EndIf
	SkipSpace(pStr)
	While *pStr
		If *pStr = Asc(";") Then Exit While '如果是注释则退出
		If *pStr = VK_RETURN Then Exit While
		If *pStr = Asc("'") Then '如果是字符串，则转换成十六进制
			pStr += 1
			While *pStr
				If *pStr = Asc("'") Then Exit While  
				If *pStr = Asc(",") Then Exit While   '
				If *pStr = VK_RETURN Then Exit While  '
			#ifdef _PRINT_CODE
				printf("%2x,",Asc(*pStr))
			#EndIf
				AddAsmData("DB",Asc(*pStr),curAddr)
				curAddr += 1
				pStr += 1
			Wend
			If *pStr = Asc("'") Then pStr += 1 '跳过单引号
			SkipSpace(pStr)
			If *pStr = Asc(",") Then pStr += 1 '跳过分隔符
		EndIf
		SkipSpace(pStr)
		n = GetWord(pStr)
		If n Then
			ReDim szBuf1(n + 1) As UByte
			AddUperWord(pStr,@szBuf1(0),n)
		#ifdef _PRINT_CODE
			printf(*Cast(ZString Ptr,@szBuf1(0)) & ",")
		#endif 
			If GetAddress(@szBuf1(0),n) Then
				AddAsmData("DB",n,curAddr)
				curAddr += 1
			EndIf
			SkipSpace(pStr)
			If *pStr = Asc(",") Then pStr += 1 '跳过分隔符
			Continue While
		EndIf
		Exit While
	Wend
	printf(!"\r\n")
End Sub

'DW 只能定义2字节的字符串，或者2字节的数据，或者1字节的数据（实际存储2字节，高字节为0）
Sub OperandIsDW(ByRef pStr As ZString Ptr)																				'DW					;...
	Dim n As Long
#ifdef _PRINT_CODE
	printf(!"%3d  \tDW\t",nline)
#EndIf
	SkipSpace(pStr)
	While *pStr
		If *pStr = Asc(";") Then Exit While '如果是注释则退出
		If *pStr = VK_RETURN Then Exit While
		If *pStr = 10 Then Exit While
		If *pStr = Asc("'") Then '如果是字符串，则转换成十六进制
			Dim tmp(Any) As UByte
			n = 0
			pStr += 1
			While *pStr
				If *pStr = Asc("'") Then Exit While  
				If *pStr = Asc(",") Then Exit While   '
				If *pStr = VK_RETURN Then Exit While  '
				If *pStr = 10 Then Exit While
				ReDim tmp(n) As UByte
				tmp(n) = Asc(*pStr)
				n += 1
				pStr += 1
			Wend
			Select Case n
			Case 0
			#ifdef _PRINT_CODE
				printf("%2x,%2x,",0,0)
			#endif
				AddAsmData("DW",0,curAddr)
				AddAsmData("DW",0,curAddr)
				curAddr += 2
			Case 1
			#ifdef _PRINT_CODE
				printf("%2x,%2x,",0,tmp(0))
			#endif
				AddAsmData("DW",0,curAddr)
				AddAsmData("DW",tmp(0),curAddr)
				curAddr += 2
			Case 2
			#ifdef _PRINT_CODE
				printf("%2x,%2x,",tmp(0),tmp(1))
			#endif
				AddAsmData("DW",tmp(0),curAddr)
				AddAsmData("DW",tmp(1),curAddr)
				curAddr += 2
			Case Else
			#ifdef _PRINT_ERRORS
				printf(!"第%d行:DW定义的字符串过大，只允许2个字符.\r\n",nline)
			#endif	
			End Select				
			If *pStr = Asc("'") Then pStr += 1 '跳过单引号
			SkipSpace(pStr)
			If *pStr = Asc(",") Then pStr += 1 '跳过分隔符
		EndIf
		SkipSpace(pStr)
		n = GetWord(pStr)
		If n Then
			ReDim szBuf1(n + 1) As UByte
			AddUperWord(pStr,@szBuf1(0),n)
		#ifdef _PRINT_CODE
			printf(*Cast(ZString Ptr,@szBuf1(0)) & ",")
		#endif 
			If GetAddress(@szBuf1(0),n) Then
				AddAsmData("DW",(n Shr 8) And &HFF,curAddr)
				AddAsmData("DW",n And &HFF,curAddr + 1)
				curAddr += 2
			EndIf
			SkipSpace(pStr)
			If *pStr = Asc(",") Then pStr += 1 '跳过分隔符
			Continue While
		EndIf
		Exit While
	Wend
	printf(!"\r\n")
End Sub

'todo
'目前全部按地址计算，实际应该区分是EQU、DATA、XDATA、BIT、SFR、SBIT，不同的定义地址范围不同
Sub OperandIsEQU(ByRef pStr As ZString Ptr,ByVal pBuf As ZString Ptr)													'... EQU ...		;
	Dim szBuf2(Any) As UByte
	If GetUperWordToBuf(pStr,szBuf2()) Then
		AddEquDatas(pBuf,@szBuf2(0),ENUM_EQU)
#ifdef _PRINT_ERRORS
	Else
		printf(!"EQU定义地址单元不存在.\r\n")
#endif
	EndIf
#ifdef _PRINT_CODE
	printf(!"%3d  \t" & *pBuf & !"\tEQU\t" & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#EndIf	
End Sub

Sub OperandIsDATA(ByRef pStr As ZString Ptr,ByVal pBuf As ZString Ptr)													'... DATA ...		;
	Dim szBuf2(Any) As UByte
	If GetUperWordToBuf(pStr,szBuf2()) Then
		AddEquDatas(pBuf,@szBuf2(0),ENUM_DATA)
#ifdef _PRINT_ERRORS
	Else
		printf(!"DATA定义地址单元不存在.\r\n")
#endif
	EndIf
#ifdef _PRINT_CODE
	printf(!"%3d  \t" & *pBuf & !"\tDATA\t" & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
End Sub

Sub OperandIsXDATA(ByRef pStr As ZString Ptr,ByVal pBuf As ZString Ptr)													'... DATA ...		;
	Dim n As Long
	Dim szBuf2(Any) As UByte
	n = GetUperWordToBuf(pStr,szBuf2())
	If n Then
		AddEquDatas(pBuf,@szBuf2(0),ENUM_XDATA)
#ifdef _PRINT_ERRORS
	Else
		printf(!"XDATA定义地址单元不存在.\r\n")
#endif
	EndIf
#ifdef _PRINT_CODE
	printf(!"%3d  \t" & *pBuf & !"\tDATA\t" & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
End Sub

Sub OperandIsBIT(ByRef pStr As ZString Ptr,ByVal pBuf As ZString Ptr)													'... BIT ...
	Dim szBuf2(Any) As UByte
	If GetUperWordToBuf(pStr,szBuf2()) Then
		AddEquDatas(pBuf,@szBuf2(0),ENUM_BIT)
#ifdef _PRINT_ERRORS
	Else
		printf(!"BIT定义地址单元不存在.\r\n")
#endif
	EndIf
#ifdef _PRINT_CODE
	printf(!"%3d  \t" & *pBuf & !"\tBIT\t" & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
End Sub

Sub OperandIsSFR(ByRef pStr As ZString Ptr,ByVal pBuf As ZString Ptr)													'... SFR ...
	Dim szBuf2(Any) As UByte 
	If GetUperWordToBuf(pStr,szBuf2()) Then
		AddEquDatas(pBuf,@szBuf2(0),ENUM_SFR)
#ifdef _PRINT_ERRORS
	Else
		printf(!"SFR定义地址单元不存在.\r\n")
#endif
	EndIf
#ifdef _PRINT_CODE
	printf(!"%3d  \t" & *pBuf & !"\tSFR\t" & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
End Sub
	
Sub OperandIsSBIT(ByRef pStr As ZString Ptr,ByVal pBuf As ZString Ptr)													'... SBIT ...
	Dim szBuf2(Any) As UByte
	If GetUperWordToBuf(pStr,szBuf2()) Then
		AddEquDatas(pBuf,@szBuf2(0),ENUM_SBIT)
#ifdef _PRINT_ERRORS
	Else
		printf(!"SBIT定义地址单元不存在.\r\n")
#endif
	EndIf
#ifdef _PRINT_CODE
	printf(!"%3d  \t" & *pBuf & !"\tSBIT\t" & *Cast(ZString Ptr,@szBuf2(0)) & Chr(13,10),nline)
#endif
End Sub

Sub OperandIsEnd()
#ifdef _PRINT_CODE	
	printf(!"%3d  \tEnd\r\n",nline)
#endif
	AddAsmData("END",0,curAddr)
'---------------------------------------------------------------------------------------------------------------------
'跳转表到遇到END后再统一处理（因为有的标签在前，有的在后，没法在循环里直接判断）
	For i As Long = 0 To JmpLabelCnt - 1
		For j As Long = 0 To LabelCnt - 1
			If JmpLabels(i).szLabel & ":" = Labels(j).szLabel Then '
				Select Case JmpLabels(i).szOperand
				Case "AJMP" 'aaa0 0001,aaaa aaaa	;2
					AsmData(JmpLabels(i).Idx).Operand = &H01 Or Cast(UByte,(Labels(j).Addr Shr 8) And &HE0)
					AsmData(JmpLabels(i).Idx + 1).Operand = Cast(UByte,Labels(j).Addr And &HFF)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx - 1).Operand,AsmData(JmpLabels(i).Idx).Operand,AsmData(JmpLabels(i).Idx + 1).Operand) 
				#endif
				Case "ACALL"'aaa1 0001,aaaa aaaa	;2
					AsmData(JmpLabels(i).Idx).Operand = &H11 Or Cast(UByte,(Labels(j).Addr Shr 8) And &HE0)
					AsmData(JmpLabels(i).Idx + 1).Operand = Cast(UByte,Labels(j).Addr And &HFF)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx - 1).Operand,AsmData(JmpLabels(i).Idx).Operand,AsmData(JmpLabels(i).Idx + 1).Operand)  
				#endif
				Case "SJMP" 'rel = 					;2
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 2)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "JMP"  'rel =					;2
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 2)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "LJMP" 'aaaa aaaa aaaa aaaa	;3
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,(Labels(j).Addr Shr 8) And &HFF)
					AsmData(JmpLabels(i).Idx + 1).Operand = Cast(UByte,Labels(j).Addr And &HFF)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx - 1).Operand,AsmData(JmpLabels(i).Idx).Operand,AsmData(JmpLabels(i).Idx + 1).Operand) 
				#endif
				Case "LCALL"'aaaa aaaa aaaa aaaa	;3			
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,(Labels(j).Addr Shr 8) And &HFF)
					AsmData(JmpLabels(i).Idx + 1).Operand = Cast(UByte,Labels(j).Addr And &HFF)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx - 1).Operand,AsmData(JmpLabels(i).Idx).Operand,AsmData(JmpLabels(i).Idx + 1).Operand)  
				#endif
				Case "CJNE" 'rel = 					;3
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 3)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif	
				Case "DJNZ" 'rel = 					;2
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 2)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "JZ"	'rel = 					;2
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 2)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "JNZ"	'rel = 					;2
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 2)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "JC"	'rel = 					;2
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 2)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "JNC"	'rel = 					;2
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 2)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "JB"	'rel = 					;3
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 3)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-2).Operand,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "JNB"	'rel = 					;3
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 3)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-2).Operand,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "JBC"	'rel = 					;3
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,Labels(j).Addr - JmpLabels(i).Addr - 3)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx-1).Operand,AsmData(JmpLabels(i).Idx).Operand) 
				#endif
				Case "MOV_DPTR"
					AsmData(JmpLabels(i).Idx).Operand = Cast(UByte,(Labels(j).Addr Shr 8) And &HFF)
					AsmData(JmpLabels(i).Idx + 1).Operand = Cast(UByte,Labels(j).Addr And &HFF)
				#ifdef _PRINT_OUTCODE
					printf(!"current addr:%4x:  %2x,%2x,%2x\r\n",JmpLabels(i).Addr,AsmData(JmpLabels(i).Idx - 1).Operand,AsmData(JmpLabels(i).Idx).Operand,AsmData(JmpLabels(i).Idx + 1).Operand) 
				#endif
				End Select
			EndIf
		Next
	Next
	IsEnd = TRUE '程序结束标志
End Sub

Sub ParseAsmFile(ByRef asmFile As Const ZString)
	If Dir(asmFile) <> "" Then
		Dim asmPath As String = asmFile
		If InStr(asmFile,Any "\/") = 0 Then
			asmPath = ExePath & "\" & asmFile
		EndIf
		'printf("文件：" & asmPath & Chr(13,10))
		
		Dim f As Integer = FreeFile
		Dim sText(Any) As UByte
		Dim pStr As ZString Ptr
		Open asmPath For Binary As #f
		If LOF(f) > 0 Then
			ReDim sText(LOF(f) + 1) As UByte
			If Get(#f,,sText()) <> 0 Then sText(0) = 0
		End If
		Close f
		pStr = @sText(0)
		nline = 0
		While *pStr
'{			'1.跳过空行	
			SkipSpace(pStr)
			If *pStr = VK_RETURN Then
			#ifdef _PRINT_SPACE
				printf(!"%3d  ;该行为空行\r\n",nline)
			#endif
				nline += 1
				pStr += 1
				If *pStr = 10 Then pStr += 1 
				Continue While
			EndIf
'}
'{			'2.解析头文件
			If *pStr = Asc("#") Then	'如果是#开头，则判断是不是头文件
				Dim szFile As String = FindInclude(asmFile,pStr)
				printf("%3d  " & szFile & Chr(13,10),nline)
				If szFile <> "" Then
					ParseAsmFile(szFile)
					Continue While
				EndIf
				SkipSpace(pStr)
			EndIf
'}
'{			'3.解析ASM文件
			If *pStr = Asc(";") Then '如果是注释，跳过注释
			#ifdef _PRINT_COMMON
				printf(!"%3d  ;该行为注释\r\n",nline)
			#endif
				'不处理，直接到末尾跳过一行就好了
			Else
				Dim n As Long = GetWord(pStr)
				If n Then
					Dim szBuf(Any) As UByte
					If *(pStr + n - 1) = Asc(":") Then '如果是标签，则记录标签，并取标签后面的代码（如果有）
						ReDim szBuf(n + 1) As UByte
						AddUperWord(pStr,@szBuf(0),n)
						AddLabels(@szBuf(0),curAddr)
					#ifdef _PRINT_CODE	
						printf("%3d  " & *Cast(ZString Ptr,@szBuf(0)) & !" Addr = %4x\r\n",nline,curAddr)
					#endif
						'获取标签后面的字符（如果有）
						SkipSpace(pStr)
						n = GetWord(pStr)
					EndIf
					If n Then
						ReDim szBuf(n + 1) As UByte
						AddUperWord(pStr,@szBuf(0),n)
						Select Case *Cast(ZString Ptr,@szBuf(0))
						Case "ORG"
							OperandIsORG(pStr)							
						Case "MOV"
							OperandIsMOV(pStr)
						Case "MOVC"
							OperandIsMOVC(pStr)
						Case "MOVX"
							OperandIsMOVX(pStr)
						Case "XCH"
							OperandIsXCH(pStr)
						Case "XCHD"
							OperandIsXCHD(pStr)
						Case "ANL"
							OperandIsANL(pStr)
						Case "ORL"
							OperandIsORL(pStr)
						Case "XRL"
							OperandIsXRL(pStr)
						Case "ADD"
							OperandIsADD(pStr)
						Case "ADDC"
							OperandIsADDC(pStr)
						Case "SUBB" 	
							OperandIsSUBB(pStr)
						Case "CLR"
							OperandIsCLR(pStr)
						Case "INC"
							OperandIsINC(pStr)
						Case "DEC"	
							OperandIsDEC(pStr)
						Case "PUSH"
							OperandIsPUSH(pStr)
						Case "POP"	
							OperandIsPOP(pStr)
						Case "SETB"
							OperandIsSETB(pStr)
						Case "CPL"
							OperandIsCPL(pStr)
						Case "RL"
							OperandIsRL(pStr)
						Case "RLC"
							OperandIsRLC(pStr)
						Case "RR"
							OperandIsRR(pStr)
						Case "RRC"
							OperandIsRRC(pStr)
						Case "SWAP"
							OperandIsSWAP(pStr)
						Case "MUL"
							OperandIsMUL(pStr)
						Case "DIV"
							OperandIsDIV(pStr)
						Case "DA"
							OperandIsDA(pStr)
						Case "AJMP"
							OperandIsAJMP(pStr)
						Case "LJMP"
							OperandIsLJMP(pStr)
						Case "SJMP"
							OperandIsSJMP(pStr)
						Case "JMP"
							OperandIsJMP(pStr)
						Case "JZ"
							OperandIsJZ(pStr)
						Case "JNZ"
							OperandIsJNZ(pStr)
						Case "JC"
							OperandIsJC(pStr)
						Case "JNC"
							OperandIsJNC(pStr)
						Case "ACALL"
							OperandIsACALL(pStr)
						Case "LCALL"
							OperandIsLCALL(pStr)
						Case "JB"
							OperandIsJB(pStr)
						Case "JNB"
							OperandIsJNB(pStr)
						Case "JBC"
							OperandIsJBC(pStr)
						Case "DJNZ"
							OperandIsDJNZ(pStr)
						Case "CJNE"
							OperandIsCJNE(pStr)
						Case "RET"
							OperandIsRET()
						Case "RETI"
							OperandIsRETI()
						Case "NOP"
							OperandIsNOP()
						Case "DB"
							OperandIsDB(pStr)
						Case "DW"
							OperandIsDW(pStr)
						Case "END"
							OperandIsEnd()
						Case Else
							Dim n1 As Long
							Dim szBuf1(Any) As UByte
							n1 = GetUperWordToBuf(pStr,szBuf1())
							If n1 Then
								Select Case *Cast(ZString Ptr,@szBuf1(0))
								Case "EQU"
									OperandIsEQU(pStr,@szBuf(0))
								Case "DATA"
									OperandIsDATA(pStr,@szBuf(0))
								Case "XDATA"
									OperandIsXDATA(pStr,@szBuf(0))
								Case "BIT"
									OperandIsBIT(pStr,@szBuf(0))
								Case "SFR"
									OperandIsSFR(pStr,@szBuf(0))
								Case "SBIT"
									OperandIsSBIT(pStr,@szBuf(0))
								Case Else
									printf(!"第%d行 未知操作符.\r\n",nline)
								End Select
							Else
								printf(!"第%d行 未知操作符.\r\n",nline)
							EndIf
						End Select
					EndIf
				EndIf
			EndIf
'}
			nline += 1
			SkipLine(pStr)
		Wend
	Else
		Print "不存在该文件。"
	EndIf	
End Sub

Function FormatToHex(ByVal nValue As ULongInt,ByVal n As Long = 2) As String
	Return Right("0000000000" & Hex(nValue),n)
End Function

Sub CompileToHex(ByRef hexFile As Const ZString)	
	'00' Data Record						//数据记录
	'01' End of File Record					//文件结束记录
	'02' Extended Segment Address Record	//扩展段地址记录
	'03' Start Segment Address Record		//开始段地址记录
	'04' Extended Linear Address Record		//扩展线性地址记录
	'05' Start Linear Address Record		//开始线性地址记录
	
	Dim s As String
	Dim HexStr As String
	Dim sum As UByte
	Dim cnt As Long
	For i As Long = 0 To AsmCnt - 1
		If AsmData(i).szAsm = "ORG" Then
			If i <> 0 Then 
				sum += cnt 
				HexStr &= ":" & FormatToHex(cnt) & s & FormatToHex(&H100 - sum) & Chr(13,10)
			EndIf
			sum = (AsmData(i).Addr Shr 8) And &HFF 
			sum += AsmData(i).Addr And &HFF
			s = FormatToHex(AsmData(i).Addr,4) & "00"
			cnt = 0
		ElseIf AsmData(i).szAsm = "END" Then
			sum += cnt
			HexStr &= ":" & FormatToHex(cnt) & s & FormatToHex(&H100 - sum) & Chr(13,10)
			HexStr &= ":00000001FF"
			Exit For
		Else
			sum += AsmData(i).Operand
			s &= FormatToHex(AsmData(i).Operand)
			cnt += 1
			If cnt >= 16 Then
				sum += cnt
				HexStr &= ":" & FormatToHex(cnt) & s & FormatToHex(&H100 - sum) & Chr(13,10)
				If i + 1 < AsmCnt Then
					sum = (AsmData(i + 1).Addr Shr 8) And &HFF 
					sum += AsmData(i + 1).Addr And &HFF
					s = FormatToHex(AsmData(i + 1).Addr,4) & "00"
				EndIf
				cnt = 0
			EndIf
		EndIf
	Next
#ifdef _PRINT_OUTCODE
	Print "Hex Code is:"
	Print Hexstr
#endif
	
	Dim hexPath As String = hexFile
	If Dir(hexFile) <> "" Then
		If InStr(hexFile,Any "\/") = 0 Then
			hexPath = ExePath & "\" & hexFile
		EndIf
	EndIf
	'printf("文件：" & hexPath & Chr(13,10))
	Dim f As Integer = FreeFile
	Open hexPath For Output As #f
	Print #f,Hexstr
	Close
End Sub

Private Function main(ByVal argc As Integer,ByVal argv As ZString Ptr Ptr) As Integer
	If argc > 1 Then '有参数
	'Parse cmd
'{  添加8051默认寄存器
		AddEquDatas("P0","80H",ENUM_SFR)
		AddEquDatas("SP","81H",ENUM_SFR)
		AddEquDatas("DPL","82H",ENUM_SFR)
		AddEquDatas("DPH","83H",ENUM_SFR)
		AddEquDatas("PCON","87H",ENUM_SFR)
		AddEquDatas("TCON","88H",ENUM_SFR)		'-TCON-
		AddEquDatas("IT0","88H",ENUM_SBIT)		'--|
		AddEquDatas("IE0","89H",ENUM_SBIT)		'--|
		AddEquDatas("IT1","8AH",ENUM_SBIT)		'--|
		AddEquDatas("IE1","8BH",ENUM_SBIT)		'--|
		AddEquDatas("TR0","8CH",ENUM_SBIT)		'--|
		AddEquDatas("TF0","8DH",ENUM_SBIT)		'--|
		AddEquDatas("TR1","8EH",ENUM_SBIT)		'--|
		AddEquDatas("TF1","8FH",ENUM_SBIT)		'--|
		AddEquDatas("TMOD","89H",ENUM_SFR)
		AddEquDatas("TL0","8AH",ENUM_SFR)
		AddEquDatas("TL1","8BH",ENUM_SFR)
		AddEquDatas("TH0","8CH",ENUM_SFR)
		AddEquDatas("TH1","8DH",ENUM_SFR)
		AddEquDatas("P1","90H",ENUM_SFR)
		AddEquDatas("SCON","98H",ENUM_SFR)		'-SCON-	
		AddEquDatas("RI","98H",ENUM_SBIT)		'--|
		AddEquDatas("TI","99H",ENUM_SBIT)		'--|
		AddEquDatas("RB8","9AH",ENUM_SBIT)		'--|
		AddEquDatas("TB8","9BH",ENUM_SBIT)		'--|
		AddEquDatas("REN","9CH",ENUM_SBIT)		'--|
		AddEquDatas("SM2","9DH",ENUM_SBIT)		'--|
		AddEquDatas("SM1","9EH",ENUM_SBIT)		'--|
		AddEquDatas("SM0","9FH",ENUM_SBIT)		'--|
		AddEquDatas("SBUF","99H",ENUM_SFR)
		AddEquDatas("P2","0A0H",ENUM_SFR)
		AddEquDatas("IE","0A8H",ENUM_SFR)		'-IE-	
		AddEquDatas("EX0","0A8H",ENUM_SBIT)		'--|
		AddEquDatas("ET0","0A9H",ENUM_SBIT)		'--|
		AddEquDatas("EX1","0AAH",ENUM_SBIT)		'--|
		AddEquDatas("ET1","0ABH",ENUM_SBIT)		'--|
		AddEquDatas("ES","0ACH",ENUM_SBIT)		'--|	
		AddEquDatas("EA","0AFH",ENUM_SBIT)		'--|
		AddEquDatas("P3","0B0H",ENUM_SFR)
		AddEquDatas("IP","0B8H",ENUM_SFR)		'-IP-	
		AddEquDatas("PX0","0B8H",ENUM_SBIT)		'--|
		AddEquDatas("PT0","0B9H",ENUM_SBIT)		'--|
		AddEquDatas("PX1","0BAH",ENUM_SBIT)		'--|
		AddEquDatas("PT1","0BBH",ENUM_SBIT)		'--|
		AddEquDatas("PS","0BCH",ENUM_SBIT)		'--|
		AddEquDatas("PSW","0D0H",ENUM_SFR)		'-PSW-
		AddEquDatas("P","0D0H",ENUM_SBIT)		'--|
		AddEquDatas("OV","0D2H",ENUM_SBIT)		'--|
		AddEquDatas("RS0","0D3H",ENUM_SBIT)		'--|
		AddEquDatas("RS1","0D4H",ENUM_SBIT)		'--|
		AddEquDatas("F0","0D5H",ENUM_SBIT)		'--|
		AddEquDatas("AC","0D6H",ENUM_SBIT)		'--|
		AddEquDatas("CY","0D7H",ENUM_SBIT)		'--|
		AddEquDatas("ACC","0E0H",ENUM_SFR)
		AddEquDatas("B","0F0H",ENUM_SFR)		
'}	
	'Parse File
		'Print argc,*argv[1],*argv[2]
		ParseAsmFile(*argv[1])
		CompileToHex(*argv[2])
	EndIf
	Return 0
End Function

'{ start
End main(__FB_ARGC__, __FB_ARGV__)
'}









