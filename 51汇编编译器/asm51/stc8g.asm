;----------------------------------------------------------------------------------
;include file
BUSY        BIT     20H.0
WPTR        DATA    21H
RPTR        DATA    22H
BUFFER      DATA    23H                         ;16 bytes

;P0			SFR		80H
;SP			SFR		81H
;DPL		SFR		82H
;DPH		SFR		83H
;PCON		SFR		87H

;TCON		SFR		88H
;IT0		SBIT	88H
;IE0		SBIT	89H
;IT1		SBIT	8AH
;IE1		SBIT	8BH
;TR0		SBIT	8CH
;TF0		SBIT	8DH
;TR1		SBIT	8EH
;TF1		SBIT	8FH

;TMOD		SFR		89H
;TL0		SFR		8AH
;TL1		SFR		8BH
;TH0		SFR		8CH
;TH1		SFR		8DH
AUXR        DATA    8EH
;P1			SFR		90H
P1M1        DATA    091H
P1M0        DATA    092H
P0M1        DATA    093H
P0M0        DATA    094H
P2M1        DATA    095H
P2M0        DATA    096H

;SCON		SFR		98H
;RI 		SBIT	98H
;TI 		SBIT	99H
;RB8		SBIT	9AH
;TB8		SBIT	9BH
;REN		SBIT	9CH
;SM2		SBIT	9DH
;SM1		SBIT	9EH
;SM0		SBIT	9FH
	
;SBUF		SFR		99H

;P2			SFR		A0H

;IE			SFR		A8H
;EX0		SBIT	A8H
;ET0		SBIT	A9H
;EX1		SBIT	AAH
;ET1		SBIT	ABH
;ES 		SBIT	ACH
;EA 		SBIT	AFH

;P3			SFR		B0H
;RXD		SBIT	B0H
;TXD		SBIT	B1H
;INT0		SBIT	B2H
;INT1		SBIT	B3H
;T0 		SBIT	B4H
;T1 		SBIT	B5H
;WR 		SBIT	B6H
;RD 		SBIT	B7H

P3M1        DATA    0B1H
P3M0        DATA    0B2H
P4M1        DATA    0B3H
P4M0        DATA    0B4H

;IP			SFR		B8H
;PX0		SBIT	B8H
;PT0		SBIT	B9H
;PX1		SBIT	BAH
;PT1		SBIT	BBH
;PS 		SBIT	BCH

P5M1        DATA    0C9H
P5M0        DATA    0CAH
	
;PSW		SFR		D0H
;P			SBIT	D0H
;OV			SBIT	D2H
;RS0		SBIT	D3H
;RS1		SBIT	D4H
;F0			SBIT	D5H
;AC			SBIT	D6H
;CY			SBIT	D7H

T2H         DATA    0D6H
T2L         DATA    0D7H

;A			SFR		E0H		;���� ACC
;B			SFR		F0H
;----------------------------------------------------------------------------------
;source file
				ORG     0000H
				LJMP    MAIN						;02 01 4B			;0000
				ORG     0023H
				LJMP    UART_ISR					;02 01 00			;0023
				ORG     0100H
UART_ISR:   	PUSH    ACC							;C0 E0				;0100 - 0101	;0100
				PUSH    PSW							;C0 D0				;0102 - 0103
				MOV     PSW,#08H					;75 D0 08			;0104 - 0106
				JNB     TI,CHKRI					;30 99 04			;0107 - 0109				;rel = 010E - 010A = 04
				CLR     TI							;C2 99				;010A - 010B
				CLR     BUSY						;C2 00				;010C - 010D
CHKRI:      	JNB     RI,UARTISR_EXIT				;30 98 0D			;010E - 0110	;0110		;rel = 011E - 0111 = 0D
				CLR     RI							;C2 98				;0111 -	0112				
				MOV     A,WPTR						;E5 21				;0113 - 0114
				ANL     A,#0FH						;54 0F				;0115 - 0116
				ADD     A,#BUFFER					;24 23				;0117 - 0118
				MOV     R0,A						;F8					;0119 
				MOV     @R0,SBUF					;A6 99				;011A - 011B
				INC     WPTR						;05 21				;011C - 011D
UARTISR_EXIT:	POP     PSW							;D0 D0				;011E - 011F
				POP     ACC							;D0 E0				;0120 - 0121	;0120
				RETI								;32					;0122 
UART_INIT:  	MOV     SCON,#50H					;75 98 50			;0123 - 0125
				MOV     T2L,#0E8H           		;75 D7 E8			;0126 - 0128
				MOV     T2H,#0FFH					;75 D6 FF			;0129 - 012B
				MOV     AUXR,#15H					;75 8E 15			;012C - 012E
				CLR     BUSY						;C2 00				;012F - 0130	;0130
				MOV     WPTR,#00H					;75 21 00			;0131 - 0133	
				MOV     RPTR,#00H					;75 22 00			;0134 - 0136
				RET									;22					;0137
UART_SEND:  	JB      BUSY,$						;20 00 FD			;0138 - 013A				;
				SETB    BUSY						;D2 00				;013B - 013C
				MOV     SBUF,A						;F5 99				;013D - 013E
				RET									;22					;013F
UART_SENDSTR:	CLR     A							;E4					;0140			;0140
				MOVC    A,@A+DPTR					;93					;0141
				JZ      SENDEND						;60 06				;0142 - 0143				;rel = 014A - 0144 = 06			
				LCALL   UART_SEND					;12 01 38			;0144 - 0146				;addr16 = 0138
				INC     DPTR						;A3					;0147
				JMP     UART_SENDSTR				;80 F6				;0148 - 0149				;rel = 0140 - 014A = F6 = -10
SENDEND:    	RET									;22					;014A
MAIN:       	MOV     SP, #5FH					;75 81 5F			;014B - 014D
				MOV     P0M0, #00H					;75 94 00			;014E - 0150	;0150
				MOV     P0M1, #00H					;75 93 00			;0151 - 0153
				MOV     P1M0, #00H					;75 92 00			;0154 - 0156
				MOV     P1M1, #00H					;75 91 00			;0157 - 0159
				MOV     P2M0, #00H					;75 96 00			;015A - 015C
				MOV     P2M1, #00H					;75 95 00			;015D - 015F
				MOV     P3M0, #00H					;75 B2 00			;0160 - 0162	;0160
				MOV     P3M1, #00H					;75 B1 00			;0163 - 0165
				MOV     P4M0, #00H					;75 B4 00			;0166 - 0168
				MOV     P4M1, #00H					;75 B3 00			;0169 - 016B
				MOV     P5M0, #00H					;75 CA 00			;016C - 016E
				MOV     P5M1, #00H					;75 C9 00			;016F - 0171	;0170
				LCALL   UART_INIT					;12 01 23			;0172 - 0174				;addr16 = 0123
				SETB    ES							;D2 AC				;0175 - 0176
				SETB    EA							;D2 AF				;0177 - 0178
				MOV     DPTR,#STRING				;90 01 96			;0179 - 017B
				LCALL   UART_SENDSTR				;12 01 40			;017C - 017E				;addr16 = 0140
LOOP:       	MOV     A,RPTR						;E5 22				;017F - 0180	;0180
				XRL     A,WPTR						;65 21				;0181 - 0182
				ANL     A,#0FH						;54 0F				;0183 - 0184
				JZ      LOOP						;60 F8				;0185 - 0186				;rel = 017F - 0187 = F8 = -8 
				MOV     A,RPTR						;E5 22				;0187 - 0188
				ANL     A,#0FH						;54 0F				;0189 - 018A	
				ADD     A,#BUFFER					;24 23				;018B - 018C
				MOV     R0,A						;F8					;018D
				MOV     A,@R0						;E6					;018E
				LCALL   UART_SEND					;12 01 38			;018F - 0191	;0190		;addr16 = 0138
				INC     RPTR						;05 22				;0192 - 0193
				JMP     LOOP						;80 E9				;0194 - 0195				;rel = 017F - 0196 = E9 = -23
STRING:     	DB      'Uart Test !',0DH,0AH,00H	;55 61 72 74 20 	;0196 - 019A
				END									;54 65 73 74 20 	;019B - 019F	
													;21 0D 0A 00		;01A0 - 01A3	;01A0



