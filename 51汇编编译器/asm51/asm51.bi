#ifndef __ASM51_BI__
#define __ASM51_BI__

'#define _PRINT_COMMON		'��ʾע����
'#define _PRINT_SPACE		'��ʾ����
#define _PRINT_CODE		'��ʾ�����Ļ���ַ���
#define _PRINT_OUTCODE		'��ʾ��ַ�������루����ת��ǩ��
#define _PRINT_ERRORS		'��ʾ������Ϣ


Type LabelType
	szLabel As String
	Addr As Long
End Type

Type JmpLabelType
	szOperand As String
	szLabel As String
	Addr As Long
	Idx As Long
End Type


Enum EquDataEnum
	ENUM_NULL = 0
	ENUM_EQU
	ENUM_DATA
	ENUM_XDATA
	ENUM_BIT
	ENUM_SFR
	ENUM_SBIT
End Enum

Type EquDataType
	szEquName As String
	szData As String
	nType As EquDataEnum = ENUM_NULL
End Type

Type AsmDataType
	szAsm As String
	Operand As UByte
	Addr As Long
End Type

#endif 
