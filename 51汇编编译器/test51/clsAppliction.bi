#ifndef __CLSAPPLICTION_BI__
#define __CLSAPPLICTION_BI__
#include Once "windows.bi"

Type clsAppliction
#ifndef UNICODE
	CompanyName As ZString * MAX_PATH      '
	ProductName As ZString * MAX_PATH      '
    ExeName     As ZString * MAX_PATH      '
    Path        As ZString * MAX_PATH      '
#else
	CompanyName As WString * MAX_PATH      '
	ProductName As WString * MAX_PATH      '
    ExeName     As WString * MAX_PATH      '
    Path        As WString * MAX_PATH      '
#endif	
	hInstance   As HINSTANCE               ' 
    Major       As Long                    '
    Minor       As Long                    '
    Revision    As Long                    '
    Build       As Long                    '

	Declare Constructor	
End Type

Extern App As clsAppliction

#endif
 
    