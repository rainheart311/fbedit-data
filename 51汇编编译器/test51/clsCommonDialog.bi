#Include Once "win\commdlg.bi"
#Include Once "win\cderr.bi" 
#include Once "win/shobjidl.bi"

Const DefMaxFontSize = 72
Const DefMinFontSize = 8

Type clsCommonDialog
Private: '打开、保存、浏览器对话框
#Ifndef UNICODE
    m_DefaultExt   As ZString * MAX_PATH
    m_DialogTitle  As ZString * MAX_PATH
    m_FileName     As ZString * MAX_PATH
    m_FileNames(Any) As ZString * MAX_PATH
    m_Filter       As ZString * MAX_PATH
    m_FileTitle    As ZString * MAX_PATH
    m_InitDir      As ZString * MAX_PATH
#Else
    m_DefaultExt   As WString * MAX_PATH
    m_DialogTitle  As WString * MAX_PATH
    m_FileName     As WString * MAX_PATH
    m_FileNames(Any) As WString * MAX_PATH
    m_Filter       As WString * MAX_PATH
    m_FileTitle    As WString * MAX_PATH
    m_InitDir      As WString * MAX_PATH
#EndIf
    m_FilterIndex  As Long = 1
    m_Flags        As DWORD              '
    m_MultiSelect  As Boolean '多选标志  '
    m_CreatePrompt As Boolean '新建提示  '                                    
Private:
    m_Color    As COLORREF 
Private: '打印对话框
    m_Copies As WORD
    m_FromPage As WORD
    m_ToPage As WORD
    m_MinPage As WORD
    m_MaxPage As WORD
Private: '字体对话框
#Ifndef UNICODE
    m_FontName       As ZString * MAX_PATH
#Else
    m_FontName       As WString * MAX_PATH
#EndIf
    m_FontSize       As Integer 
    m_FontItalic     As Boolean
    m_FontBold       As Boolean
    m_FontStrikethru As Boolean
    m_FontUnderline  As Boolean
Private:
    Declare sub GetFileName(sFileName As LPCTSTR) 
Public:
    Declare Constructor()
    Declare Destructor()
    Declare Property Flags() As DWORD                                    '标志位
    Declare Property Flags(nFlags As DWORD)
'打开、保存对话框Flags选项
    'cdlOFNReadOnly           &H1      建立对话框时，只读复选框初始化为选定。该标志也指示对话框关闭时只读复选框的状态。
    'cdlOFNOverwritePrompt    &H2      使“另存为”对话框当选择的文件已经存在时应产生一个信息框，用户必须确认是否覆盖该文件。
    'cdlOFNHideReadOnly       &H4      隐藏只读复选框。
    'cdlOFNNoChangeDir        &H8      强制对话框将对话框打开时的目录置成当前目录。
    'cdlOFNHelpButton         &H10     使对话框显示帮助按钮。
    'cdlOFNNoValidate         &H100    它指定公共对话框允许返回的文件名中含有非法字符。
    'cdlOFNAllowMultiselect   &H200    它指定文件名列表框允许多重选择。
    'cdlOFNExtensionDifferent &H400    它指示返回的文件扩展名与 DefaultExt 属性指定的扩展名不一致。
    'cdlOFNPathMustExist      &H800    它指定只能输入有效路径。如果设置该标志，输入非法路径时，应显示一个警告信息。
    'cdlOFNFileMustExist      &H1000   它指定只能输入文件名文本框已经存在的文件名。
    'cdlOFNCreatePrompt       &H2000   当文件不存在时对话框要提示创建文件。该标志自动设置 cdlOFNPathMustExist 和 cdlOFNFileMustExist 标志。
    'cdlOFNShareAware         &H4000   它指定忽略共享冲突错误。
    'cdlOFNNoReadOnlyReturn   &H8000   它指定返回的文件不能具有只读属性，也不能在写保护目录下面。
    'cdlOFNNoLongNames        &H40000  无长文件名。
    'cdlOFNExplorer           &H80000  它使用类似资源管理器的打开一个文件的对话框模板。
    'cdlOFNNoDereferenceLinks &H100000 不要间接引用外壳链接（也称作快捷方式）。
    'cdlOFNLongNames          &H200000 使用长文件名。
    
'颜色对话框Flags选项
    'CC_ANYCOLOR                       对话框显示所有可用的基于基本颜色的颜色。
    'CC_ENABLEHOOK                     激活由lpfnHook成员指定的钩子程序。该标识仅用于初始化对话框时。
    'CC_ENABLETEMPLATE                 表示利用由hInstance 和 lpTemplateName 成员指定的对话框模板。该标识符仅用于初始化对话框。
    'CC_ENABLETEMPLATEHANDLE           表示hInstance成员标识一个包含了预加载的对话框模板的数据块。如果该标识被指定，系统会忽略 lpTemplateName 成员。该标识符仅用于初始化对话框。
    'CC_FULLOPEN                       让对话框显示额外的控件以使用户创建自定义的颜色。如果该标识未设置，用户必须点击【自定义颜色】按钮才能显示自定颜色控件。
    'CC_PREVENTFULLOPEN                使自定义颜色按钮失效。
    'CC_RGBINIT                        让对话框默认使用由rgbResult成员指定的颜色
    'CC_SHOWHELP                       让对话框显示帮助按钮。hwndOwner成员必须在用户点击【帮助】按钮时发送HELPMSGSTRING消息的窗口
    'CC_SOLIDCOLOR                     让对话框仅显示基本颜色组成的纯色。
'字体对话框Flags选项
    'cdlCFScreenFonts（屏幕字体） 
    'cdlCFPrinterFonts（打印机字体） 
    'cdlCFBoth（既可以是屏幕字体又可以是打印机字体） 
Public:
'打开保存对话框
    Declare Function ShowOpen() As Boolean                               ' 
    Declare Function ShowSave() As Boolean    
#Ifndef UNICODE
    Declare Property DefaultExt() ByRef As ZString                       '缺省扩展名
#Else
    Declare Property DefaultExt() ByRef As WString                    
#EndIf
    Declare Property DefaultExt(lpszText As LPCTSTR)             
    
#Ifndef UNICODE
    Declare Property DialogTitle() ByRef As ZString                      '对话框标题
#Else
    Declare Property DialogTitle() ByRef As WString                     
#EndIf
    Declare Property DialogTitle(lpszText As LPCTSTR)              
    
#Ifndef UNICODE
    Declare Property FileName() ByRef As ZString                         '文件名称
    Declare Property FileName(ByVal Index As Integer) ByRef As ZString   '
#Else
    Declare Property FileName() ByRef As WString                         
    Declare Property FileName(Index As Integer) ByRef As WString         '
#EndIf
    Declare Property FileName(lpszText As LPCTSTR)                
    
#Ifndef UNICODE
    Declare Property Filter() ByRef As ZString                           '过滤器
#Else
    Declare Property Filter() ByRef As WString                           
#EndIf
    Declare Property Filter(lpszText As LPCTSTR)                   
    
#Ifndef UNICODE
    Declare Property FileTitle() ByRef As ZString                        '文件标题
#Else
    Declare Property FileTitle() ByRef As WString                        
#EndIf
    Declare Property FileTitle(lpszText As LPCTSTR)                
    
#Ifndef UNICODE
    Declare Property InitDir() ByRef As ZString                          '初始化路径
#Else
    Declare Property InitDir() ByRef As WString                          
#EndIf
    Declare Property InitDir(lpszText As LPCTSTR)                  

    Declare Property MultiSelect() As Boolean                         '多选标志
    Declare Property MultiSelect(IsMulti As Boolean)
    Declare Property FilterIndex() As Long                         '指定默认的文件过滤器
    Declare Property FilterIndex(Index As Long)

'浏览器对话框
    Declare Function ShowBrowse() As Boolean   '
    Declare Function FolderExists(sFileName As LPCTSTR) As Boolean '
    Declare Function CreateFolder(sFileName As LPCTSTR) As Boolean '

'颜色对话框
    Declare Function ShowColor() As Boolean                        '
    Declare Property Color() As COLORREF                           '颜色
    Declare Property Color(nColor As COLORREF) 
'打印机选项对话框    
    Declare Function ShowPrinter() As Boolean                      '
    Declare Property Copies() As WORD                              '打印份数
    Declare Property Copies(ByVal wTxt As WORD)
    Declare Property FromPage() As WORD                            '起始页
    Declare Property FromPage(ByVal wTxt As WORD)
    Declare Property ToPage() As WORD                              '结束页
    Declare Property ToPage(ByVal wTxt As WORD)
    Declare Property MinPage() As WORD                             '最小页
    Declare Property MinPage(ByVal wTxt As WORD)
    Declare Property MaxPage() As WORD                             '最大页
    Declare Property MaxPage(ByVal wTxt As WORD)
    Declare Property IsAllPages() As Boolean                       '是否打印所有页面
    Declare Property IsAllPages(nAllPages As Boolean)
'字体对话框
    Declare Function ShowFont() As Boolean                         '
#Ifndef UNICODE
    Declare Property FontName() ByRef As ZString                   '
#Else
    Declare Property FontName() ByRef As WString                   '
#EndIf
    Declare Property FontName(lpszText As LPCTSTR)
    Declare Property FontSize() As Integer                         '
    Declare Property FontSize(nSize As Integer)
    Declare Property FontItalic() As Boolean                       '
    Declare Property FontItalic(nItalic As Boolean)
    Declare Property FontBold() As Boolean                         '
    Declare Property FontBold(nBold As Boolean)
    Declare Property FontStrikethru() As Boolean                   '
    Declare Property FontStrikethru(nStrikethru As Boolean)
    Declare Property FontUnderline() As Boolean                    '
    Declare Property FontUnderline(nUnderline As Boolean)      
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
'
Constructor clsCommonDialog
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
    m_Copies = 1
    m_FromPage = 1
    m_ToPage = 1
    m_MinPage = 1
    m_MaxPage = 1
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
    m_DialogTitle = "对话框"
    m_FileName = ""
    m_FileTitle = ""
    m_InitDir = Left(App.Path,Len(App.Path) - 1)
    m_Filter = "所有文件(*.*)|*.*"
    m_MultiSelect = FALSE 
    m_Flags = OFN_EXPLORER Or OFN_FILEMUSTEXIST Or OFN_HIDEREADONLY 
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'    
#Ifndef UNICODE
    m_FontName = "宋体"
#Else
    m_FontName = WStr("宋体")
#EndIf
    m_FontSize = 9
    m_Color = &H000000 '默认黑色
    
    m_FontItalic = False
    m_FontBold = False
    m_FontStrikethru = False
    m_FontUnderline = False
End Constructor

Destructor clsCommonDialog

End Destructor
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'设置颜色对话框
Function clsCommonDialog.ShowColor() As Boolean
    Dim ColorSpec As ChooseColor, lCustomColor(15) As Long
    ColorSpec.lStructSize  = SizeOf(ColorSpec)
    ColorSpec.HWNDOwner    = NULL
    For i As Long = 0 To 15
        lCustomColor(i) = BGR(0,i * 16, (15 - i) * 16)
    Next
    ColorSpec.lpCustColors = VarPtr(lCustomColor(0))
    ColorSpec.rgbResult = m_Color
    ColorSpec.Flags = ColorSpec.Flags Or CC_RGBINIT Or CC_FULLOPEN
    If ChooseColor(@ColorSpec) Then
    	m_Color = Colorspec.rgbResult
    Else
    	m_Color = -1
    	Function = FALSE
    EndIf
End Function
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
Function clsCommonDialog.ShowPrinter() As Boolean
    Dim pd As PrintDlg
    
    pd.lStructSize = SizeOf(pd)
    pd.HWNDOwner = NULL
    pd.flags = m_Flags Or PD_SHOWHELP
    pd.nCopies = m_Copies
    pd.nFromPage = MAX(m_FromPage, m_MinPage)
    pd.nToPage = MAX(MIN(m_ToPage, m_MaxPage), m_MaxPage)
    pd.nMinPage = m_MinPage
    pd.nMaxPage = m_MaxPage
    If PrintDlg(@pd) Then
        'hdc = pd.hDC
        m_Copies = pd.nCopies
        m_FromPage = pd.nFromPage
        m_ToPage = pd.nToPage
        m_Flags = pd.flags
        m_MinPage = pd.nMinPage
        m_MaxPage = pd.nMaxPage
        If pd.hDevMode  Then GlobalFree(pd.hDevMode)
        If pd.hDevNames Then GlobalFree(pd.hDevNames)
        Return TRUE
    End If
    Return FALSE
End Function
    
Property clsCommonDialog.Copies() As WORD             '打印份数
    Return m_Copies
End Property

Property clsCommonDialog.Copies(ByVal wTxt As WORD)
    m_Copies = wTxt
End Property
    
Property clsCommonDialog.FromPage() As WORD             '起始页
    Return m_FromPage
End Property

Property clsCommonDialog.FromPage(ByVal wTxt As WORD)
    m_FromPage = wTxt
End Property
    
Property clsCommonDialog.ToPage() As WORD             '结束页
    Return m_ToPage
End Property

Property clsCommonDialog.ToPage(ByVal wTxt As WORD)
    m_ToPage = wTxt
End Property
    
Property clsCommonDialog.MinPage() As WORD             '最小页
    Return m_MinPage
End Property

Property clsCommonDialog.MinPage(ByVal wTxt As WORD)
    m_MinPage = wTxt
End Property
    
Property clsCommonDialog.MaxPage() As WORD             '最大页
    Return m_MaxPage
End Property

Property clsCommonDialog.MaxPage(ByVal wTxt As WORD)
    m_MaxPage = wTxt 
End Property

Property clsCommonDialog.IsAllPages() As Boolean                       '是否打印所有页面
    Return m_Flags And PD_ALLPAGES
End Property

Property clsCommonDialog.IsAllPages(nAllPages As Boolean)
    If nAllPages Then
    	m_Flags And= PD_ALLPAGES
    Else
    	m_Flags Or= Not(PD_ALLPAGES)
    EndIf
End Property

'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
Function clsCommonDialog.ShowOpen() As Boolean     
#Ifndef UNICODE
    Dim tempFile As ZString * 23768 '64kb
#Else
    Dim tempFile As WString * 23768 '64kb
#EndIf
'设置Filter
    If Right(m_Filter, 1) <> "|" Then m_Filter = m_Filter & "|" 
    Dim pchar As TCHAR Ptr = @m_Filter
    For i As Long = 0 To Len(m_Filter) - 1
        If pchar[i] = Asc("|") Then pchar[i] = 0  '将"|"替换为空字符
    Next
'设置InitDir
    If Len(m_InitDir) = 0 Then m_InitDir = CurDir  '如果默认路径为空，则设置当前路径
    m_InitDir = m_InitDir & Chr(0) '确保是以空字符结尾
'设置FileName
    memcpy(@tempFile,@m_FileName,Len(m_FileName))

   ' // Fill the members of the structure
    Dim ofn As OPENFILENAME
    ofn.lStructSize     = SizeOf(ofn)
    'If AfxWindowsVersion < 5 THEN ofn.lStructSize = 76
    ofn.HWNDOwner       = NULL
    ofn.lpstrFilter     = @m_Filter
    ofn.nFilterIndex    = m_FilterIndex
    ofn.lpstrFile       = @tempFile
    ofn.nMaxFile        = SizeOf(tempFile)
    ofn.lpstrFileTitle    = @m_FileTitle
    ofn.nMaxFileTitle     = SizeOf(m_FileTitle)
    ofn.lpstrInitialDir = @m_InitDir
    If Len(m_DialogTitle) Then ofn.lpstrTitle = @m_DialogTitle
    ofn.Flags = m_Flags Or OFN_EXPLORER
    If Len(m_DefaultExt) Then ofn.lpstrDefExt = @m_DefaultExt

    If GetOpenFileName(@ofn) Then 
    	If m_Flags And OFN_ALLOWMULTISELECT = OFN_ALLOWMULTISELECT Then
    	    GetFileName(tempFile)
    	    m_FileName = m_FileNames(0)
    	Else
    		m_FileName = RTrim(tempFile,Chr(32))
    	EndIf
        Function = TRUE
    Else
        m_FileName = ""
        Function = FALSE
    End If 
    If m_Flags Then m_Flags = ofn.Flags
End Function

Function clsCommonDialog.ShowSave() As Boolean 
#Ifndef UNICODE
    Dim tempFile As ZString * 23768 '64kb
#Else
    Dim tempFile As WString * 23768 '64kb
#EndIf
	Dim Ofn As OPENFILENAME
	
'设置Filter
    If Right(m_Filter, 1) <> "|" Then m_Filter = m_Filter & "|" 
    Dim pchar As TCHAR Ptr = @m_Filter
    For i As Long = 0 To Len(m_Filter) - 1
        If pchar[i] = Asc("|") Then pchar[i] = 0  '将"|"替换为空字符
    Next
    m_Filter            = m_Filter & Chr(0)
'设置InitDir
    If Len(m_InitDir) = 0 Then m_InitDir = CurDir  '如果默认路径为空，则设置当前路径
    m_InitDir = m_InitDir & Chr(0) '确保是以空字符结尾
'
    m_DefaultExt = m_DefaultExt & Chr(0)
    m_DialogTitle = m_DialogTitle & Chr(0)

    ofn.lStructSize       = SizeOf(OPENFILENAME) 
    ofn.hWndOwner         = NULL
    ofn.lpstrFilter       = @m_Filter
    ofn.nFilterIndex      = 1
    ofn.lpstrFile         = @tempFile
    ofn.nMaxFile          = SizeOf(tempFile)
    ofn.lpstrFileTitle    = VarPtr(m_FileTitle)
    ofn.nMaxFileTitle     = SizeOf(m_FileTitle)
    ofn.lpstrInitialDir   = @m_InitDir
    If Len(m_DialogTitle) Then ofn.lpstrTitle = @m_DialogTitle
    ofn.Flags             = m_Flags Or OFN_EXPLORER
    ofn.lpstrDefExt       = @m_DefaultExt
  
    If GetSaveFileName(@ofn) Then
    	m_FileName = RTrim(tempFile,Chr(32))
    	Function = TRUE
    Else
    	m_FileName = ""
    	Function = FALSE
    EndIf
    If m_Flags Then m_Flags = ofn.Flags
End Function
      
#Ifndef UNICODE
Property clsCommonDialog.DialogTitle() ByRef As ZString  
#Else
Property clsCommonDialog.DialogTitle() ByRef As WString
#EndIf 
    Return m_DialogTitle
End Property
                '
Property clsCommonDialog.DialogTitle(lpszText As LPCTSTR)
    m_DialogTitle = *lpszText
End Property
    
#Ifndef UNICODE
Property clsCommonDialog.FileName() ByRef As ZString    
#Else
Property clsCommonDialog.FileName() ByRef As WString
#EndIf
    Return m_FileName
End Property

Sub clsCommonDialog.GetFileName(sFileName As LPCTSTR) 
    Dim As Integer i,j = Len(*sFileName),k = 0
    
    Do 
        i = j
        j = InStrRev(*sFileName,Chr(0),i - 1)
        
        #Ifndef UNICODE
		    ReDim Preserve m_FileNames(k) As ZString * MAX_PATH
		#Else
		    ReDim Preserve m_FileNames(k) As WString * MAX_PATH
		#EndIf
        m_FileNames(k) = Mid(*sFileName,j + 1,i - j)
        
        k += 1
    Loop While j <> 0
End Sub

#Ifndef UNICODE
Property clsCommonDialog.FileName(Index As Integer) ByRef As ZString 
#Else
Property clsCommonDialog.FileName(Index As Integer) ByRef As WString 
#EndIf    
    Return m_FileNames(Index)
End Property
               '
Property clsCommonDialog.FileName(lpszText As LPCTSTR)
    m_FileName = *lpszText
End Property
    
#Ifndef UNICODE
Property clsCommonDialog.FileTitle() ByRef As ZString 
#Else
Property clsCommonDialog.FileTitle() ByRef As WString 
#EndIf
    m_FileTitle = m_FileName
    Dim nPos As Long = InstrRev(m_FileName,Any ":/\")
    If nPos Then m_FileTitle = Mid(m_FileName, nPos + 1)
    nPos = InstrRev(m_FileTitle, ".")
    If nPos Then m_FileTitle = Mid(m_FileTitle, 1, nPos - 1)
    Return m_FileTitle
End Property
                '
Property clsCommonDialog.FileTitle(lpszText As LPCTSTR)
    m_FileTitle = *lpszText
End Property
    
#Ifndef UNICODE
Property clsCommonDialog.InitDir() ByRef As ZString   
#Else
Property clsCommonDialog.InitDir() ByRef As WString
#EndIf
    Return m_InitDir
End Property
                '
Property clsCommonDialog.InitDir(lpszText As LPCTSTR)
    If Right(*lpszText,1) = "\" Then 
    	m_InitDir = Left(*lpszText,Len(*lpszText) - 1)
    Else
    	m_InitDir = *lpszText
    EndIf
End Property
    
#Ifndef UNICODE
Property clsCommonDialog.Filter() ByRef As ZString    
#Else
Property clsCommonDialog.Filter() ByRef As WString
#EndIf
    Return m_Filter
End Property
                '
Property clsCommonDialog.Filter(lpszText As LPCTSTR)
    m_Filter = *lpszText
End Property
    
Property clsCommonDialog.MultiSelect() As Boolean   
    Return m_MultiSelect
End Property           

Property clsCommonDialog.MultiSelect(IsMulti As Boolean)  
    m_MultiSelect = IsMulti
    If IsMulti = FALSE Then
        m_Flags = OFN_EXPLORER Or OFN_FILEMUSTEXIST Or OFN_HIDEREADONLY
    Else
        m_Flags = OFN_EXPLORER Or OFN_FILEMUSTEXIST Or OFN_HIDEREADONLY Or OFN_ALLOWMULTISELECT
    End If
End Property 

Property clsCommonDialog.FilterIndex() As Long                         '指定默认的文件过滤器
    Return m_FilterIndex
End Property 

Property clsCommonDialog.FilterIndex(Index As Long)
    m_FilterIndex = Index
End Property 
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
'Function MyBrowseFolderProc(ByVal hwnd As HWND,ByVal uMsg As UINT,ByVal wParam As WPARAM,ByVal lParam As LPARAM) As Integer
'#Ifndef UNICODE
'    Dim zBuffer As ZString * MAX_PATH
'#Else
'    Dim zBuffer As WString * MAX_PATH
'#EndIf
'    Select Case uMsg 
'       Case BFFM_INITIALIZED 
'          SendMessage hWnd, BFFM_SETSELECTION, True, lParam
'       Case BFFM_SELCHANGED 
'          SHGetPathFromIDList Cast(Any Ptr, wParam), zBuffer
'          If (wParam=0) Or _                          ' No id number
'             (Len(zBuffer)=0) Or _                    ' No name
'             (Mid(zBuffer,2,1) <> ":") Then           ' Not a local or mapped drive
'             SendMessage hWnd, BFFM_ENABLEOK, False, False
'          End If
'    End Select
'    Return 0  
'End Function
'
'Function clsCommonDialog.ShowBrowse() As Boolean
'    Dim zBuffer    As ZString * MAX_PATH
'    Dim bi         As BROWSEINFO
'    Dim lpIDList   As HANDLE
'    
'    CoInitialize NULL
'    
'    If m_Flags = 0 Then m_Flags = BIF_RETURNONLYFSDIRS OR BIF_DONTGOBELOWDOMAIN OR BIF_USENEWUI OR BIF_RETURNFSANCESTORS
' 
'    bi.HWNDOwner    = NULL
'    bi.lpszTitle    = @m_DialogTitle
'    bi.ulFlags      = BIF_RETURNONLYFSDIRS Or BIF_DONTGOBELOWDOMAIN Or BIF_RETURNFSANCESTORS Or BIF_USENEWUI 
'    bi.lpfn         = Cast(BFFCALLBACK, @MyBrowseFolderProc)
'    bi.lParam       = Cast(LPARAM,@m_InitDir)
'    lpIDList        = SHBrowseForFolder(VarPtr(bi))
' 
'    If lpIDList Then
'        Function = SHGetPathFromIDList(Cast(Any Ptr, lpIDList),@m_FileName)
'        CoTaskMemFree lpIDList
'    End If   
'    CoUninitialize 
'End Function

'Function clsCommonDialog.CreateFolder(sFileName As LPCTSTR) As Boolean 
'    m_FileName = Replace(*sFileName,"\\","\")  '确保没有双斜杠    
'    Return MkDir(m_FileName)
'End Function
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'设置字体对话框
Function clsCommonDialog.ShowFont() As Boolean
    Dim tChsFnt As ChooseFont, tLogFnt As Logfont
    
    Dim m_hDC As hDC = GetDC(0)  '获取DC
'设置逻辑字体参数（）    
    With tLogFnt
        .lfHeight = -MulDiv(m_FontSize, GetDeviceCaps(m_hDC, LOGPIXELSY), 72)    '字体高度，跟字体字体大小相关
        .lfWidth = 0                                                             '字体宽度
        .lfEscapement = 0                                                        ' angle between baseline and escapement vector
        .lfOrientation = 0                                                       ' angle between baseline and orientation vector
        .lfWeight = IIf(m_FontBold,FW_BOLD,FW_NORMAL)                            '字体粗细，默认FW_NORMAL，加粗为FW_BOLD
        .lfCharSet = DEFAULT_CHARSET                                             '使用默认字符集
        .lfOutPrecision = OUT_DEFAULT_PRECIS                                     ' default precision mapping
        .lfClipPrecision = CLIP_DEFAULT_PRECIS                                   ' default clipping precision
        .lfQuality = DEFAULT_QUALITY                                             ' default quality setting
        .lfPitchAndFamily = DEFAULT_PITCH Or FF_ROMAN                            ' default pitch, proportional with serifs
        .lfFaceName = m_FontName                                                 '字体，默认为Times New Roman（新罗马字体）
        .lfItalic = m_FontItalic                                                 '是否斜体
        .lfUnderline = m_FontUnderline                                           '是否有下划线
        .lfStrikeOut = m_FontStrikethru                                          '是否有删除线
    End With

'设置选择字体对话框信息    
    With tChsFnt
        .lStructSize = Len(tChsFnt)     '结构体大小                             ' size of structure
        .HWNDOwner = NULL               '所有窗口（打开字体对话框的窗口）       
        .hDC = m_hDC                    'DC(device context)从窗口句柄获得
        .lpLogFont = @tLogFnt           '这个是指针，需要地址引用，指向逻辑字体结构
        .iPointSize = m_FontSize * 10   '默认12号（小四）字体
        .Flags = CF_BOTH Or CF_EFFECTS Or CF_FORCEFONTEXIST Or CF_INITTOLOGFONTSTRUCT Or CF_LIMITSIZE '所有功能使用
        .rgbColors = m_Color        '默认黑色
        .nFontType = REGULAR_FONTTYPE   ' regular font type i.e. not bold or anything
        .nSizeMin = DefMinFontSize      '最小字号，默认8
        .nSizeMax = DefMaxFontSize      '最大字号，默认72
    End With

    If ChooseFont(@tChsFnt) Then  '打开选择字体对话框，成功返回非0（真）
        m_FontName = tLogFnt.lfFaceName
        m_FontItalic = Cast(Boolean,tLogFnt.lfItalic)   
        m_FontStrikethru = Cast(Boolean,tLogFnt.lfStrikeOut) 
        m_FontUnderline = Cast(Boolean,tLogFnt.lfUnderline) 
        
        Select Case tLogFnt.lfWeight '字体粗细
            Case FW_DONTCARE  '
            Case FW_THIN  '
            Case FW_EXTRALIGHT '
            Case FW_LIGHT '
            Case FW_NORMAL '正常(普通)
                m_FontBold = False
            Case FW_MEDIUM '
            Case FW_SEMIBOLD '
            Case FW_BOLD    '加粗
                m_FontBold = True
            Case FW_EXTRABOLD '
            Case FW_HEAVY '
            Case Else
        End Select
        m_FontSize = tChsFnt.iPointSize / 10 '
        m_Color = tChsFnt.rgbColors
        
        ShowFont = True
    Else
        ShowFont = False
    End If
    
    ReleaseDC(0,m_hDC)
End Function

'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
#Ifndef UNICODE
Property clsCommonDialog.FontName() ByRef As ZString                   '
#Else
Property clsCommonDialog.FontName() ByRef As WString                   '
#EndIf
    Return m_FontName  
End Property 

Property clsCommonDialog.FontName(lpszText As LPCTSTR)
    m_FontName = *lpszText
End Property 
    
Property clsCommonDialog.FontSize() As Integer                    '
    Return m_FontSize
End Property 

Property clsCommonDialog.FontSize(nSize as Integer)
    m_FontSize = nSize
End Property 
    
Property clsCommonDialog.FontItalic() as Boolean               '
    Return m_FontItalic   
End Property 

Property clsCommonDialog.FontItalic(nItalic as Boolean)
    m_FontItalic = nItalic
End Property 
    
Property clsCommonDialog.FontBold() as Boolean               '
    Return m_FontBold
End Property 

Property clsCommonDialog.FontBold(nBold as Boolean)
    m_FontBold = nBold
End Property 
    
Property clsCommonDialog.FontStrikethru() as Boolean               '
    Return m_FontStrikethru
End Property 

Property clsCommonDialog.FontStrikethru(nStrikethru as Boolean)
    m_FontStrikethru = nStrikethru
End Property 
    
Property clsCommonDialog.FontUnderline() as Boolean               '
    Return m_FontUnderline
End Property 

Property clsCommonDialog.FontUnderline(nUnderline as Boolean)
    m_FontUnderline = nUnderline
End Property 

Property clsCommonDialog.Color() as COLORREF                 ' 
    Return m_Color
End Property 

Property clsCommonDialog.Color(nColor as COLORREF)
    m_Color = nColor
End Property   







