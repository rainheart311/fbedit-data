#include Once "clsForm.bi"

Sub clsForm.Center(ByVal hParent As HWND = NULL)
	Dim rc As RECT    		' Window coordinates
    Dim rcParent As RECT    ' Parent window coordinates
    Dim rcWorkArea As RECT  ' Work area coordinates
    Dim pt As POINT   		' x and y coordinates of centered window

   	' // Get the coordinates of the window
    GetWindowRect m_hWnd, @rc
	' // Calculate the width and height of the window
    Dim nWidth As Long = rc.Right - rc.Left
    Dim nHeight As Long = rc.Bottom - rc.Top
	' // Get the coordinates of the work area
    If SystemParametersInfo(SPI_GETWORKAREA,SizeOf(rcWorkArea),@rcWorkArea,0) = 0 Then
        rcWorkArea.Right  = GetSystemMetrics(SM_CXSCREEN)
        rcWorkArea.Bottom = GetSystemMetrics(SM_CYSCREEN)
    End If
    ' // Get the coordinates of the parent window
    If hParent Then
        GetWindowRect hParent, @rcParent
    Else
        rcParent.Left   = rcWorkArea.Left
        rcParent.Top    = rcWorkArea.Top
        rcParent.Right  = rcWorkArea.Right
        rcParent.Bottom = rcWorkArea.Bottom
    End If
	' // Calculate the width and height of the parent window
    Dim nParentWidth As Long = rcParent.Right - rcParent.Left
    Dim nParentHeight As Long = rcParent.Bottom - rcParent.Top
	' // Calculate the new x coordinate and adjust for work area
    pt.x = rcParent.Left + ((nParentWidth - nWidth) \ 2)
    If (pt.x < rcWorkArea.Left) Then
        pt.x = rcWorkArea.Left
    ElseIf ((pt.x + nWidth) > rcWorkArea.Right) Then
        pt.x = rcWorkArea.Right - nWidth
    End If
	' // Calculate the new y coordinate and adjust for work area
    pt.y = rcParent.Top  + ((nParentHeight - nHeight) \ 2)
    If (pt.y < rcWorkArea.Top) Then
        pt.y = rcWorkArea.Top
    ElseIf ((pt.y + nHeight) > rcWorkArea.Bottom) Then
        pt.y = rcWorkArea.Bottom - nHeight
    End If
	' // Convert screen coordinates to client area coordinates
    If (GetWindowLongPtr(m_hwnd,GWL_STYLE) And WS_CHILD) = WS_CHILD Then ScreenToClient hParent, @pt
	' // Reposition the window retaining its size and Z order
    SetWindowPos(m_hwnd,NULL,pt.x,pt.y,0,0,SWP_NOSIZE OR SWP_NOZORDER)
End Sub

Sub clsForm.Close() 
	PostMessage m_hWnd,WM_CLOSE,0,0
End Sub

Sub clsForm.PopupMenu(ByVal hMenu As HMENU,ByVal x As Long,ByVal y As Long)
	TrackPopupMenu(hMenu,TPM_LEFTALIGN Or TPM_RIGHTBUTTON,x,y,0,m_hWnd,0)
End Sub

Property clsForm.Icon() As HICON
	Return m_Icon
End Property

Property clsForm.Icon(ByVal nValue As HICON)
	m_Icon = nValue
    If IsWindow(m_hWnd) Then
    	SendMessage m_hWnd,WM_SETICON,ICON_BIG,Cast(LPARAM,m_Icon)
    	SendMessage m_hWnd,WM_SETICON,ICON_SMALL,Cast(LPARAM,m_Icon)
    EndIf
End Property

Property clsForm.ScaleLeft() As Long
	Dim rc As RECT
    GetClientRect(m_hWnd,@rc)     '获得客户区大小
    Return rc.Left
End Property

Property clsForm.ScaleTop() As Long
	Dim rc As RECT
    GetClientRect(m_hWnd,@rc)     '获得客户区大小
    Return rc.Top
End Property

Property clsForm.ScaleWidth() As Long
	Dim rc As RECT
    GetClientRect(m_hWnd,@rc)     '获得客户区大小
    Return rc.Right - rc.Left 
End Property

Property clsForm.ScaleHeight() As Long
	Dim rc As RECT
    GetClientRect(m_hWnd,@rc)     '获得客户区大小
    Return rc.Bottom - rc.Top
End Property

Property clsForm.WindowState() As FormWindowState
	If IsWindow(m_hWnd) Then
	    If IsIconic(m_hWnd) Then      ' 
	        Return FormWindowState.Minimized
	    ElseIf IsZoomed(m_hWnd) Then  ' 
	        Return FormWindowState.Maximized
	    Else 
	        Return FormWindowState.Normal     ' 
	    End If
    Else
    	Return m_WindowState
    End If
End Property

Property clsForm.WindowState(ByVal nValue As FormWindowState)
	If IsWindow(m_hWnd) Then
	    Select Case nValue
    	Case FormWindowState.Normal
            ShowWindow(m_hWnd, SW_RESTORE)  ' 
    	Case FormWindowState.Minimized
            ShowWindow(m_hWnd, SW_MINIMIZE) ' 
    	Case FormWindowState.Maximized
            ShowWindow(m_hWnd, SW_MAXIMIZE) ' 
	    Case Else
	    End Select
    Else
        m_WindowState = nValue
    End If
End Property























