#Ifndef __CLSTEXTBOX_BI__
#define __CLSTEXTBOX_BI__

#include Once "clsControl.bi"

Enum TextScrollBars
   None = 0
   Horizontal
   Vertical
   Both
End Enum  

Type clsTextBox Extends clsControl	
Private:
	m_Multiline As Boolean = FALSE 
	m_ScrollBars As TextScrollBars = TextScrollBars.None
Public:
	Declare Constructor
	Declare Destructor
	
Public:	
	Declare Function Create(ByVal hParent As HWND,ByVal pszText As LPCTSTR,ByVal nid As Integer = 0,ByVal x As Long = 0,ByVal y As Long = 0,ByVal nWidth As Long = 0,ByVal nHeight As Long = 0) As HWND  
	
	Declare Sub SelClear ()
    Declare Sub SelCopy ()
    Declare Sub SelCut ()
    Declare Sub Paste ()
	Declare Sub ReplaceText(ByRef szText As String)
	
    Declare Function GetLine(ByVal nLine As Long) As String
    Declare Function GetLineNum() As Long
	Declare Sub Scroll(ByVal nValue As Long) ' 滚动多行文本框中的文本。,{1.SB_LINEDOWN 滚动下移一行。.SB_LINEUP 滚动一行。.SB_PAGEDOWN 滚动下移一页。.SB_PAGEUP 滚动一页。}
Public:		
	Declare Property Modify() As Boolean
	Declare Property Modify(ByVal nValue As Boolean) 
	Declare Property TextLimit() As Long
    Declare Property TextLimit(ByVal nValue As Long)
    Declare Property PasswordChar() As TCHAR
    Declare Property PasswordChar(ByVal nValue As TCHAR)
    Declare Property Locked() As Boolean
    Declare Property Locked(ByVal nValue As Boolean)   
    Declare Property SelStart() As Long
    Declare Property SelStart(ByVal nValue As Long)    
    Declare Property SelLength() As Long
    Declare Property SelLength(ByVal nValue As Long)     
    Declare Property MultiLine() As Boolean
    Declare Property MultiLine(ByVal nValue As Boolean)    
    Declare Property ScrollBars() As TextScrollBars              '滚动条方式 0 - None;1 - Horizontal;2 - Vertical;3 - Both
    Declare Property ScrollBars(ByVal nValue As TextScrollBars)   
	Declare Property LeftMargin() As Long
	Declare Property LeftMargin(ByVal nValue As Long) 
	Declare Property RightMargin() As Long
	Declare Property RightMargin(ByVal nValue As Long)
	Declare Property LineCount() As Long
End Type

#endif
