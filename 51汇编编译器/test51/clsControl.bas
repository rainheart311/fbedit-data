
#include Once "clsControl.bi"

Constructor clsControl
End Constructor

Destructor clsControl
End Destructor

Sub clsControl.Move(ByVal nLeft As Long, ByVal nTop As Long, ByVal nWidth As Long = 0, ByVal nHeight As Long = 0)
	If IsWindow(m_hWnd) Then
    	Dim rc As Rect
    	GetWindowRect m_hWnd, @rc          '获得窗体大小
    	If nWidth <= 0 Then nWidth = rc.Right - rc.Left
    	If nHeight <= 0  Then  nHeight = rc.Bottom - rc.Top
    	SetWindowPos(m_hWnd, 0, nLeft, nTop, nWidth, nHeight,SWP_NOZORDER Or SWP_NOACTIVATE)
    End If
End Sub

Sub clsControl.SetFocus()
	If IsWindow(m_hWnd) Then  
        .SetFocus m_hWnd
    End If
End Sub

Property clsControl.hWindow() As HWND
	Return m_hWnd
End Property

Property clsControl.hWindow(ByVal hWin As HWND)
	m_hWnd = hWin
End Property

Property clsControl.ID() As Integer
	If IsWindow(m_hWnd) Then
        Return GetDlgCtrlID(m_hWnd)
    End If
End Property

Property clsControl.Text() As String 
	If IsWindow(m_hWnd) Then
        Dim nBufferSize As Long = SendMessage(m_hWnd ,WM_GETTEXTLENGTH ,0 ,0) + 1
        Dim szText As String = Space(nBufferSize)
        SendMessage(m_hWnd,WM_GETTEXT,nBufferSize,Cast(LPARAM,StrPtr(szText)))
        Return szText 
    End If
End Property

Property clsControl.Text(ByRef szText As String) 
	If IsWindow(m_hWnd) Then          
        SetWindowText m_hWnd ,StrPtr(szText)
	End If
End Property

Property clsControl.Enabled() As Boolean  
	If IsWindow(m_hWnd) Then 
        Return IsWindowEnabled(m_hWnd)
    End If
End Property

Property clsControl.Enabled(ByVal nValue As Boolean)
	If IsWindow(m_hWnd) Then
        EnableWindow(m_hWnd,nValue)
    End If
End Property

Property clsControl.Visible() As Boolean    
	If IsWindow(m_hWnd) Then 
        Return IsWindowVisible(m_hWnd)
    End If
End Property

Property clsControl.Visible(ByVal nValue As Boolean)
	If IsWindow(m_hWnd) Then 
        ShowWindow(m_hWnd,IIf(nValue,SW_SHOW,SW_HIDE))
    End If
End Property

Property clsControl.Tag() As String
	Return m_Tag
End Property

Property clsControl.Tag(ByRef szText As String)
	m_Tag = szText
End Property

Property clsControl.Left() As Long
	If IsWindow(m_hWnd) Then
       Dim rc As Rect
       GetWindowRect m_hWnd,@rc
       MapWindowPoints NULL,GetParent(m_hWnd),Cast(LPPOINT,@rc),2
       Return rc.Left
    End If
End Property

Property clsControl.Left(ByVal nValue As Long)
	If IsWindow(m_hWnd) Then
        Dim rc As Rect
        GetWindowRect m_hWnd, @rc
        MapWindowPoints NULL,GetParent(m_hWnd),Cast(LPPOINT,@rc), 2
        SetWindowPos(m_hWnd,0,nValue,rc.top,0,0,SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOACTIVATE)
    End If
End Property

Property clsControl.Top() As Long
	If IsWindow(m_hWnd) Then
        Dim rc As Rect
        GetWindowRect m_hWnd,@rc
        MapWindowPoints NULL,GetParent(m_hWnd),Cast(LPPOINT,@rc),2
        Return rc.Top
    End If
End Property

Property clsControl.Top(ByVal nValue As Long)
	If IsWindow(m_hWnd) Then
        Dim rc As Rect
        GetWindowRect m_hWnd, @rc
        MapWindowPoints NULL, GetParent(m_hWnd), Cast(LPPOINT,@rc),2
        SetWindowPos(m_hWnd,0,rc.Left,nValue,0,0,SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOACTIVATE)
    End If
End Property

Property clsControl.Width() As Long
	If IsWindow(m_hWnd) Then
        Dim rc As Rect
        GetWindowRect m_hWnd, @rc
        MapWindowPoints NULL,GetParent(m_hWnd),Cast(LPPOINT,@rc),2
        Return rc.Right - rc.Left
    End If
End Property

Property clsControl.Width(ByVal nValue As Long)
	If IsWindow(m_hWnd) Then
        Dim rc As Rect
        GetWindowRect m_hWnd, @rc
        MapWindowPoints NULL,GetParent(m_hWnd),Cast(LPPOINT,@rc),2
        SetWindowPos(m_hWnd,0,0,0,nValue,rc.Bottom - rc.Top,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
    End If
End Property

Property clsControl.Height() As Long
	If IsWindow(m_hWnd) Then
        Dim rc As Rect
        GetWindowRect m_hWnd,@rc          ' 
        MapWindowPoints NULL,GetParent(m_hWnd),Cast(LPPOINT,@rc),2
        Return  rc.Bottom - rc.Top
    End If
End Property

Property clsControl.Height(ByVal nValue As Long)
	If IsWindow(m_hWnd) Then
        Dim rc As Rect
        GetWindowRect m_hWnd, @rc
        MapWindowPoints NULL,GetParent(m_hWnd),Cast(LPPOINT,@rc),2
        SetWindowPos(m_hWnd,0,0,0,rc.Right - rc.Left,nValue,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
    End If
End Property

Property clsControl.Font() As HFONT                  '
    If IsWindow(m_hWnd) Then  
        Return Cast(HFONT,SendMessage(m_hWnd,WM_GETFONT,0,0))
    End If
End Property

Property clsControl.Font(ByVal nValue As HFONT)
    If IsWindow(m_hWnd) Then  
        SendMessage m_hWnd,WM_SETFONT,Cast(WPARAM,nValue),TRUE
    End If
End Property





















