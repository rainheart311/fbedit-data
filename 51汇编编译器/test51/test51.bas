'#Define UNICODE
#include once "windows.bi"
#Include Once "win/commctrl.bi"
#Include Once "win/commdlg.bi"
#Include Once "win/shellapi.bi"
#Include Once "resource.bi"
#Include Once "test51.bi"
#include Once "clsTextBox.bi"
#include Once "clsAppliction.bi"
#include Once "clsForm.bi"
#include Once "clsCommonDialog.bi"

Dim Shared frmMain As clsForm
Dim Shared txtPath As clsTextBox

Function WndProc(ByVal hWin As HWND,ByVal uMsg As UINT,ByVal wParam As WPARAM,ByVal lParam As LPARAM) As Integer
	Select Case uMsg
	Case WM_INITDIALOG
		frmMain.hWindow = hWin
		txtPath.hWindow = GetDlgItem(hWin,IDC_EDT1)
		txtPath.Text = App.Path & "stc8g.asm"
	Case WM_COMMAND
		Select Case HiWord(wParam)
		Case BN_CLICKED
			Select Case LoWord(wParam)
			Case IDC_BTN1
				Dim s As String = txtPath.Text 'ĩβ�и�0
				If Exec("asm51.exe",Left(s,Len(s)-1) & " code_file.hex") = -1 Then 'txtPath.Text & 
					Print "����"
				EndIf
			Case IDC_BTN2
				Dim cd As clsCommonDialog
				cd.Filter = "Asm File|*.asm;*.s|All Files|*.*|"
				If cd.ShowOpen Then
					txtPath.Text = cd.FileName
				EndIf
			Case IDC_BTN3
				Cls
			End Select
		End Select
	Case WM_SIZE
		
	Case WM_CLOSE
		DestroyWindow(hWin)
	Case WM_DESTROY
		PostQuitMessage(NULL)
	Case Else
		Return DefWindowProc(hWin,uMsg,wParam,lParam)
	End Select
	Return 0
End Function

Function WinMain(ByVal hInst As HINSTANCE,ByVal hPrevInst As HINSTANCE,ByVal CmdLine As LPCTSTR,ByVal CmdShow As Integer) As Integer
	Dim wc As WNDCLASSEX
	Dim msg As MSG

	' Setup and register class for dialog
	wc.cbSize        = SizeOf(WNDCLASSEX)
	wc.style         = CS_HREDRAW or CS_VREDRAW
	wc.lpfnWndProc   = @WndProc
	wc.cbClsExtra    = 0
	wc.cbWndExtra    = DLGWINDOWEXTRA
	wc.hInstance     = hInst
	wc.hbrBackground = Cast(HBRUSH,COLOR_BTNFACE+1)
	wc.lpszMenuName  = NULL
	wc.lpszClassName = Cast(LPCTSTR,@ClassName)
	wc.hIcon         = LoadIcon(NULL,IDI_APPLICATION)
	wc.hIconSm       = wc.hIcon
	wc.hCursor       = LoadCursor(NULL,IDC_ARROW)
	RegisterClassEx(@wc)
	' Create and show the dialog
	CreateDialogParam(hInst,MAKEINTRESOURCE(IDD_DIALOG),NULL,@WndProc,NULL)
	ShowWindow(frmMain.hWindow,SW_SHOWNORMAL)
	UpdateWindow(frmMain.hWindow)
	' Message loop
	Do While GetMessage(@msg,NULL,0,0)
		TranslateMessage(@msg)
		DispatchMessage(@msg)
	Loop
	Return msg.wParam

End Function

'{ Program start
    InitCommonControls
    WinMain(GetModuleHandle(NULL),NULL,GetCommandLine,SW_SHOWDEFAULT)
'    
	ExitProcess(0)
    End
'}
'Program End
