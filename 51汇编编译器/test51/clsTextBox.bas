#Include Once "clsTextBox.bi"

Constructor clsTextBox
End Constructor

Destructor clsTextBox
End Destructor

Function clsTextBox.Create(ByVal hParent As HWND,ByVal pszText As LPCTSTR,ByVal nid As Integer,ByVal x As Long,ByVal y As Long,ByVal nWidth As Long,ByVal nHeight As Long) As HWND
	If IsWindow(m_hWnd) Then Exit Function
	
	Dim dwStyle As DWORD = WS_CHILD Or ES_AUTOVSCROLL Or ES_AUTOHSCROLL Or WS_VISIBLE Or WS_TABSTOP 
	
	If this.Multiline Then dwStyle = dwStyle Or ES_MULTILINE Or ES_WANTRETURN
	Select Case this.ScrollBars  '0 - None;1 - Horizontal;2 - Vertical;3 - Both
	Case TextScrollBars.Horizontal
        dwStyle = dwStyle Or WS_HSCROLL
	Case TextScrollBars.Vertical
        dwStyle = dwStyle Or WS_VSCROLL
	Case TextScrollBars.Both
        dwStyle = dwStyle Or WS_HSCROLL Or WS_VSCROLL
	Case Else
	End Select
	Dim hInst As HINSTANCE = Cast(HINSTANCE,GetWindowLongPtr(hParent,GWLP_HINSTANCE))
	m_hWnd = CreateWindowEx(WS_EX_CLIENTEDGE,"Edit",pszText,dwStyle,x,y,nWidth,nHeight,hParent,Cast(HMENU,nid),hInst,NULL)
    If m_hWnd Then
    	this.Font = Cast(HFONT,SendMessage(hParent,WM_GETFONT,0,0))
    End If
    Function = m_hWnd
End Function

Sub clsTextBox.SelClear() 
	If IsWindow(m_hWnd) Then
		SendMessage m_hWnd,WM_CLEAR,0,0
	EndIf
End Sub

Sub clsTextBox.SelCopy() 
    If IsWindow(m_hWnd) Then
    	SendMessage m_hWnd,WM_COPY,0,0
    EndIf
End Sub

Sub clsTextBox.SelCut() 
    If IsWindow(m_hWnd) Then
    	SendMessage m_hWnd,WM_CUT,0,0
    EndIf
End Sub

Sub clsTextBox.Paste() 
    If IsWindow(m_hWnd) Then
    	SendMessage m_hWnd,WM_PASTE,0,0
    EndIf
End Sub

Sub clsTextBox.ReplaceText(ByRef szText As String) 
    If IsWindow(m_hWnd) Then
    	SendMessage m_hWnd,EM_REPLACESEL,TRUE,Cast(LPARAM,StrPtr(szText))
    EndIf
End Sub

' 滚动多行文本框中的文本。{1.SB_LINEDOWN 滚动下移一行。SB_LINEUP 滚动一行。SB_PAGEDOWN 滚动下移一页。SB_PAGEUP 滚动一页。}
Sub clsTextBox.Scroll(ByVal nValue As Long)
    If IsWindow(m_hWnd) Then
    	SendMessage m_hWnd,EM_SCROLL,nValue,0
    EndIf
End Sub

Function clsTextBox.GetLine(ByVal nLine As Long) As String
    If IsWindow(m_hWnd) Then
        Dim nBufferSize As Long
    	Dim sText As String
        nBufferSize = SendMessage(m_hWnd,EM_LINELENGTH,nLine,0)
        sText = Space(nBufferSize + 1)                       
        SendMessage m_hWnd,EM_GETLINE,nLine,Cast(LPARAM,StrPtr(sText))
        Function = RTrim(sText,Chr(0))
    End If
End Function

Function clsTextBox.GetLineNum() As Long
	If IsWindow(m_hWnd) Then
		Dim n As Long = SendMessage(m_hWnd,EM_GETSEL,0,0) / 2 ^ 16
		Return SendMessage(m_hWnd,EM_LINEFROMCHAR,n,0)
	EndIf
End Function
 
Property clsTextBox.Modify() As Boolean
	If IsWindow(m_hWnd) Then
		Return SendMessage(m_hWnd,EM_GETMODIFY,0,0)
	EndIf
End Property

Property clsTextBox.Modify(ByVal nValue As Boolean) 
	If IsWindow(m_hWnd) Then
		SendMessage(m_hWnd,EM_SETMODIFY,nValue,0)
	EndIf
End Property

Property clsTextBox.TextLimit() As Long
	If IsWindow(m_hWnd) Then
		Return SendMessage(m_hWnd,EM_GETLIMITTEXT,0,0)
	EndIf
End Property

Property clsTextBox.TextLimit(ByVal nValue As Long)
	If IsWindow(m_hWnd) Then
		SendMessage m_hWnd,EM_SETLIMITTEXT,nValue,0
	EndIf
End Property

Property clsTextBox.PasswordChar() As TCHAR
	If IsWindow(m_hWnd) Then
		Return SendMessage(m_hWnd,EM_GETPASSWORDCHAR,0,0)
	EndIf
End Property

Property clsTextBox.PasswordChar(ByVal nValue As TCHAR)
	If IsWindow(m_hWnd) Then  
        SendMessage(m_hWnd,EM_SETPASSWORDCHAR,nValue,0)
    End If 
End Property

Property clsTextBox.Locked() As Boolean
	If IsWindow(m_hWnd) Then
		Return Cast(Boolean,(GetWindowLong(m_hWnd,GWL_STYLE) And ES_READONLY) = ES_READONLY)
	EndIf
End Property

Property clsTextBox.Locked(ByVal nValue As Boolean)
	If IsWindow(m_hWnd) Then
        SendMessage m_hWnd,EM_SETREADONLY,nValue,0
    End If
End Property

Property clsTextBox.SelStart() As Long
	If IsWindow(m_hWnd) Then
		Return LoWord(SendMessage(m_hWnd,EM_GETSEL,0,0))
	EndIf
End Property

Property clsTextBox.SelStart(ByVal nValue As Long) 
	If IsWindow(m_hWnd) Then
		SendMessage m_hWnd,EM_SETSEL,nValue,nValue
	EndIf
End Property

Property clsTextBox.SelLength() As Long
	If IsWindow(m_hWnd) Then
		Dim n As Long = SendMessage(m_hWnd,EM_GETSEL,0,0)
		Return HiWord(n) - LoWord(n)
	EndIf
End Property

Property clsTextBox.SelLength(ByVal nValue As Long)
	If IsWindow(m_hWnd) Then
		Dim n As Long = LoWord(SendMessage(m_hWnd,EM_GETSEL,0,0))
    	SendMessage m_hWnd, EM_SETSEL,n,n + nValue
	EndIf
End Property

Property clsTextBox.MultiLine() As Boolean
	If IsWindow(m_hWnd) Then 
        m_MultiLine = Cast(Boolean,(GetWindowLong(m_hWnd,GWL_STYLE) And ES_MULTILINE) = ES_MULTILINE) 
    End If
    Return m_MultiLine
End Property

Property clsTextBox.MultiLine(ByVal nValue As Boolean)
	m_MultiLine = nValue
End Property

'滚动条方式 0 - None;1 - Horizontal;2 - Vertical;3 - Both  
Property clsTextBox.ScrollBars() As TextScrollBars            
	If IsWindow(m_hWnd) Then     
        If (GetWindowLong(m_hWnd,GWL_STYLE) And WS_HSCROLL) = WS_HSCROLL Then
            m_ScrollBars = TextScrollBars.Horizontal
        ElseIf (GetWindowLong(m_hWnd,GWL_STYLE) And WS_VSCROLL) = WS_VSCROLL Then
            m_ScrollBars = TextScrollBars.Vertical
        ElseIf (GetWindowLong(m_hWnd,GWL_STYLE) And (WS_VSCROLL Or WS_HSCROLL)) = (WS_VSCROLL Or WS_HSCROLL) Then
            m_ScrollBars = TextScrollBars.Both
        Else
            m_ScrollBars = TextScrollBars.None
        End If  
    End If
    Return m_ScrollBars
End Property

Property clsTextBox.ScrollBars(ByVal nValue As TextScrollBars) 
	If nValue < TextScrollBars.None OrElse nValue > TextScrollBars.Both Then nValue = TextScrollBars.None
    m_ScrollBars = nValue
End Property

Property clsTextBox.LeftMargin() As Long
	If IsWindow(m_hWnd) Then
		Return LoWord(SendMessage(m_hWnd,EM_GETMARGINS,0,0))
	EndIf
End Property

Property clsTextBox.LeftMargin(ByVal nValue As Long)
	If IsWindow(m_hWnd) Then
		SendMessage m_hWnd,EM_SETMARGINS,EC_LEFTMARGIN,nValue
	EndIf
End Property

Property clsTextBox.RightMargin() As Long
	If IsWindow(m_hWnd) Then
		Return HiWord(SendMessage(m_hWnd,EM_GETMARGINS,0,0))
	EndIf
End Property

Property clsTextBox.RightMargin(ByVal nValue As Long)
	If IsWindow(m_hWnd) Then
		SendMessage m_hWnd,EM_SETMARGINS,EC_RIGHTMARGIN,nValue
	EndIf
End Property

Property clsTextBox.LineCount() As Long
	If IsWindow(m_hWnd) Then
		Return SendMessage(m_hWnd,EM_GETLINECOUNT,0,0)
	EndIf
End Property



















