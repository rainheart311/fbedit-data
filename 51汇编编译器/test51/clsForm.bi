#ifndef __CLSFORM_BI__
#define __CLSFORM_BI__
#include Once "clsControl.bi"

Enum FormWindowState
   Maximized = 1
   Minimized
   Normal
End Enum

Type clsForm Extends clsControl
Private:
	m_Icon As HICON
	m_WindowState As FormWindowState
Public:	
    Declare Sub Center(ByVal hParent As HWND = NULL)
	Declare Sub Close() 
	Declare Sub PopupMenu(ByVal hMenu As HMENU,ByVal x As Long,ByVal y As Long)
Public:	
	Declare Property Icon() As HICON
	Declare Property Icon(ByVal nValue As HICON)
    Declare Property ScaleLeft() As Long               
    Declare Property ScaleTop() As Long 
    Declare Property ScaleWidth() As Long               
    Declare Property ScaleHeight() As Long              
    Declare Property WindowState() As FormWindowState             
    Declare Property WindowState(ByVal nValue As FormWindowState)
End Type

#endif

