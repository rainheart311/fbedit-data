

#include Once "windows.bi"

Type clsControl
Private:	
	m_Tag As String
	
Protected:
	m_hWnd As HWND
	
Public:
	Declare Constructor
	Declare Destructor
	
Public:	
	Declare Sub Move(ByVal nLeft As Long, ByVal nTop As Long, ByVal nWidth As Long = 0, ByVal nHeight As Long = 0)
    Declare Sub SetFocus()
    
Public:		
	Declare Property hWindow() As HWND                 
    Declare Property hWindow(ByVal hWin As HWND)
    Declare Property ID() As Integer
    Declare Property Text() As String                   
    Declare Property Text(ByRef szText As String) 
    Declare Property Enabled() As Boolean                 
    Declare Property Enabled(ByVal nValue As Boolean)
    Declare Property Visible() As Boolean                 
    Declare Property Visible(ByVal nValue As Boolean)
    Declare Property Tag() As String
    Declare Property Tag(ByRef szText As String)
    Declare Property Left() As Long
    Declare Property Left(ByVal nValue As Long)
    Declare Property Top() As Long
    Declare Property Top(ByVal nValue As Long)
    Declare Property Width() As Long
    Declare Property Width(ByVal nValue As Long)
    Declare Property Height() As Long
    Declare Property Height(ByVal nValue As Long)	
    Declare Property Font() As HFONT
    Declare Property Font(ByVal nValue As HFONT)
End Type
