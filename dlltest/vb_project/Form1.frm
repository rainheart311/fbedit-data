VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "FB编译的DLL测试"
   ClientHeight    =   1815
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   1815
   ScaleWidth      =   4680
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command6 
      Caption         =   "设置数组"
      Height          =   615
      Left            =   3120
      TabIndex        =   5
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton Command5 
      Caption         =   "设置字符串"
      Height          =   615
      Left            =   1800
      TabIndex        =   4
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton Command4 
      Caption         =   "返回字符串"
      Height          =   615
      Left            =   480
      TabIndex        =   3
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton Command3 
      Caption         =   "调用减法"
      Height          =   615
      Left            =   1800
      TabIndex        =   2
      Top             =   240
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Caption         =   "减法错误"
      Height          =   615
      Left            =   3120
      TabIndex        =   1
      Top             =   240
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "调用加法"
      Height          =   615
      Left            =   480
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub Command1_Click()
    domsg "加法:" & Add(5, 10)
End Sub

Private Sub Command2_Click()
    domsg "减法:" & Dec(5, 10)
End Sub

Private Sub Command3_Click()
    domsg "减法:" & Dec(10, 5)
End Sub

Private Sub Command4_Click()
    domsg ver
End Sub

Private Sub Command5_Click()
    domsg "测试的字符串"
End Sub

Private Sub Command6_Click()
    Dim arr(10) As Long
    Dim i As Long
    
    Debug.Print "加载前"
    For i = 0 To UBound(arr)
        Debug.Print arr(i),
    Next
    
    Call LoadArray(VarPtr(arr(0)), UBound(arr) + 1)
    
    Debug.Print
    Debug.Print "加载后"
    For i = 0 To UBound(arr)
        Debug.Print arr(i),
    Next
End Sub
