'1、如果需要给VB直接调用，则需要加上Extern "Windows-MS" ... End Extern
'   如果给FB使用，不需要加
'2、需要导出的函数，需要加Export，不加找不到入口点
'3、如果使用VFB，则必须在工程属性里去掉 支持GDI+（默认选中），不然VB软件和VB的IDE会失去响应。

#cmdline "-x ../vb_project/dlltest.dll" '直接将DLL生成到VB工程目录下，省的手动复制
										
Extern "Windows-MS"
#include Once "windows.bi"

'使用BSTR的时候需要声明
#include Once "win/wtypes.bi"
#include Once "win/shlobj.bi"
#include Once "win/oleauto.bi"

'1、数值
Function fAdd(ByVal a As Long,ByVal b As Long) As Long Export 
	Return a + b
End Function

Function fSub(ByVal a As Long,ByVal b As Long) As Long Export 
	If a < b Then Return -1
	Return a - b
End Function

'2、字符串
'VB默认的字符串类型是BSTR，所以需要进行转换，这里提供两个转换函数（VFB里拷贝的）
Function BSTRToString(ByVal sText As BSTR) As String	'将VB里的字符串转换为FB里使用的字符串
    Dim n As Long = Peek(Long,Cast(UInteger,sText) - 4)	'获取字符串长度
    Dim s As String = String(n,0)						'分配字符串控件并初始化为0
    memcpy StrPtr(s),sText,n							'复制字符串数据
    Function = s										'返回字符串
End Function

Function StringToBSTR(ByRef sText As String) As BSTR   	'将FB里的字符串转换为VB里使用的字符串
	Function = SysAllocString(Cast(BSTR,StrPtr(sText)))	'伪造 VB 字符串
End Function

Sub fPrint(ByVal sText As BSTR) Export 
	Dim s As String = BSTRToString(sText)
	MessageBox NULL,StrPtr(s),"显示",MB_OK
End Sub

#define ver WStr("1.0.0")								'BSTR 是用WString格式存储的
Function fVersion() As BSTR Export 
	Return StringToBSTR(ver)
End Function

'3、数组
'VB与FB之间的数组结构不同，不能直接传递，需要使用指针来传递
Sub LoadArrayRnd(ByVal arr As Long Ptr,ByVal nsize As Long) Export
	Randomize											'生成随机数种子
	For i As Long = 0 To (nsize - 1)
		arr[i] = Int(Rnd() * 100) + 1					'设置随机数
	Next
End Sub

End Extern 